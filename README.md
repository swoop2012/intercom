README for domofon
==========================
Search and pagination for CRUD are working as follows:

1.  The object is needed, where the search params will be passed.
This can be a class from domain or it can be a separate class for search. 
This class can be created when the additional fields for search are required.
For instance, in user class there is a date fields and it's required to search by a period. 
dateFrom and dateTo properties can be added in order to search by a period.   
2.  Class for getting a specification has to be written. The object from point 1 should be passed
to the method for getting a specification. In this method specification instance will be created respectively to the passed params.
3.  In repository a method should be defined, 
which will take the specification from point 2 and the object of type Pageable for pagination.
4.  In resource there will be a following definition of the method:
`public Page<User> getAll(@ModelAttribute("user") User user, Pageable pageable)`
, where Spring will map the passed params to the object of type User for search(point 1)
`@ModelAttribute("user") User user`


```
Pageable pageable
```

Spring will map the passed params for pagination in the object of type Pageable. Params, which can be passed to pagination:

*  page  - page number (starting from 0);
*  size  - page size;
*  sort  - sorting params. It should be a field name and the direction of sorting (asc|desc), for example (sort=name,asc).
