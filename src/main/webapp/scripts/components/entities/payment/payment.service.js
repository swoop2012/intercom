'use strict';

angular.module('domofonApp')
    .factory('Payment', function ($resource) {
        return $resource('api/payments/:id', {}, {
            'query': { method: 'GET', isObject: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.date = new Date(data.date);
                    return data;
                }
            },
            getTypes: { method: 'GET', isArray: true, url:'api/payments/types'},
            getResults: { method: 'GET', isArray: true, url:'api/payments/results'},
            getServices: { method: 'GET', isArray: true, url:'api/payments/services'},
            getOperations: { method: 'GET', isArray: true, url:'api/payments/operations'}
        });
    });
