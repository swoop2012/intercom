'use strict';

angular.module('domofonApp')
    .factory('Entrance', function ($resource) {
        return $resource('api/entrances/:id', {}, {
            'query': { method: 'GET', isObject: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'loadAll': { method: 'GET', isArray: true, url:'/api/entrances/load'}
        });
    });
