'use strict';

angular.module('domofonApp')
    .factory('Media', function ($resource) {
        return $resource('api/medias/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            }
        });
    });
