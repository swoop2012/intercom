'use strict';

angular.module('domofonApp')
    .factory('Affiliate', function ($resource) {
        return $resource('api/affiliates/:id', {}, {
            'query': { method: 'GET', isObject: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            loadAll: { method: 'GET', isArray: true, url:'api/affiliates/load'}

        });
    });
