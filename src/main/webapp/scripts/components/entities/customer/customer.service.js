'use strict';

angular.module('domofonApp')
    .factory('Customer', function ($resource) {
        return $resource('api/customers/:id', {}, {
            'query': { method: 'GET', isObject: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.connectionDate = new Date(data.connectionDate);
                    return data;
                }
            },
            'getReceiverTypes':{ method: 'GET', isArray: true, url:'api/customers/receiver-types'},
            'getOptions': {method: 'GET', isArray: true, url:'api/customers/options'}
        });
    });
