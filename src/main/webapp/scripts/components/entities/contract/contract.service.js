'use strict';

angular.module('domofonApp')
    .factory('Contract', function ($resource) {
        return $resource('api/contracts/:id', {}, {
            'query': { method: 'GET', isObject: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.date = new Date(data.date);
                    return data;
                }
            },
            'loadAll': {method: 'GET', isArray: true, url:'api/contracts/load'}
        });
    });
