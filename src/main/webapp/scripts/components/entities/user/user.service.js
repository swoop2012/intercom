'use strict';

angular.module('domofonApp')
    .factory('User', function ($resource) {
        return $resource('api/users/:id', {}, {
            'query': { method: 'GET', isObject: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            getRoles: { method: 'GET', isArray: true, url:'api/users/roles'},
            getOptions: {method: 'GET', isArray: true, url:'api/users/options'}

        });
    });
