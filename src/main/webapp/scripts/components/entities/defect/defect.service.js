'use strict';

angular.module('domofonApp')
    .factory('Defect', function ($resource) {
        return $resource('api/defects/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            }
        });
    });
