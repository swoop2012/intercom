'use strict';

angular.module('domofonApp')
    .factory('Exempt', function ($resource) {
        return $resource('api/exempts/:id', {}, {
            'query': { method: 'GET', isObject: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.startDate = new Date(data.startDate);
                    data.endDate = new Date(data.endDate);
                    return data;
                }
            },
            loadAll: { method: 'GET', isArray: true, url:'api/exempts/load-all'},
            getTypes: { method: 'GET', isArray: true, url:'api/exempts/types'}
        });
    });
