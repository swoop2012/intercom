'use strict';

angular.module('domofonApp')
    .factory('Company', function ($resource) {
        return $resource('api/companies/:id', {}, {
            'query': { method: 'GET', isObject: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            loadAll: { method: 'GET', isArray: true, url:'api/companies/load'}
        });
    });
