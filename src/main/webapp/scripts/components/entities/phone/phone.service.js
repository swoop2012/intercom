'use strict';

angular.module('domofonApp')
    .factory('Phone', function ($resource) {
        return $resource('api/phones/:id', {}, {
            'query': { method: 'GET', isObject: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            }
        });
    });
