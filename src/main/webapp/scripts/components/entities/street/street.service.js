'use strict';

angular.module('domofonApp')
    .factory('Street', function ($resource) {
        return $resource('api/streets/:id', {}, {
            'query': { method: 'GET', isObject: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            loadAll: { method: 'GET', isArray: true, url:'api/streets/load'}
        });
    });
