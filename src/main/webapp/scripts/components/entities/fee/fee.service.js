'use strict';

angular.module('domofonApp')
    .factory('Fee', function ($resource) {
        return $resource('api/fees/:id', {}, {
            'query': { method: 'GET', isObject: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.date = new Date(data.date);
                    return data;
                }
            }
        });
    });
