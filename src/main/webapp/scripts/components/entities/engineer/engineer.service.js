'use strict';

angular.module('domofonApp')
    .factory('Engineer', function ($resource) {
        return $resource('api/engineers/:id', {}, {
            'query': { method: 'GET', isObject: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            "loadAll": { method: 'GET', isArray: true, url:'api/engineers/load'}
        });
    });
