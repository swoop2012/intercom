'use strict';

angular.module('domofonApp')
    .factory('City', function ($resource) {
        return $resource('api/cities/:id', {}, {
            'query': { method: 'GET', isObject: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            loadAll: { method: 'GET', isArray: true, url:'api/cities/load'}
        });
    });
