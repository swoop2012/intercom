'use strict';

angular.module('domofonApp')
    .factory('House', function ($resource) {
        return $resource('api/houses/:id', {}, {
            'query': { method: 'GET', isObject: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            loadAll: { method: 'GET', isArray: true, url:'api/houses/load'}
        });
    });
