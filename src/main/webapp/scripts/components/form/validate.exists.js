angular.module('domofonApp').directive('uniqueField', function($http, $parse) {
    var toId;
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, elem, attr, ctrl) {
            scope.$watch(attr.ngModel, function(value) {
                var exitsAttrs = scope.$eval(attr.uniqueField);
                var currentValue;
                exitsAttrs.id = exitsAttrs.id || 0;
                if(toId) clearTimeout(toId);
                toId = setTimeout(function(){
                    if(value && value != '')
                    {
                        if(typeof value == "object" && exitsAttrs.param)
                            currentValue = value[exitsAttrs.param];
                        else
                            currentValue = value;


                        $http.get(exitsAttrs.link + '?' + exitsAttrs.field + '=' + currentValue + '&id=' + exitsAttrs.id ).success(function(data) {
                            ctrl.$setValidity('uniqueField', true);
                        }).error(function(){
                            ctrl.$setValidity('uniqueField', false);
                        });
                    }

                }, 200);
            })
        }
    }
});
