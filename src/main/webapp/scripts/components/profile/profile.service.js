'use strict';

angular.module('domofonApp')
    .factory('Profile', function ($resource) {
        return $resource('api/profile', {}, {
            'balance': {url: 'api/profile/balance', method: 'GET', isObject: true},
            'cashFlow': {url: 'api/profile/cash-flow', method: 'GET', isObject: true},
            'cashFlowTotal': {url: 'api/profile/cash-flow-total', method: 'GET', isObject: true},
            'feeHistory': {url: 'api/profile/fee-history', method: 'GET', isObject: true},
            'paymentHistory': {url: 'api/profile/payment-history', method: 'GET', isObject: true}

        });
    });
