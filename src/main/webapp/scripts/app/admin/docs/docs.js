'use strict';

angular.module('domofonApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('docs', {
                parent: 'admin',
                url: '/docs',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/admin/docs/docs.html'
                    }
                }
            });
    });
