'use strict';

angular.module('domofonApp')
    .controller('LoginController', function ($rootScope, $scope, $state, $timeout, Auth, Principal) {
        $scope.user = {};
        $scope.errors = {};

        $scope.rememberMe = true;
        $timeout(function (){angular.element('[ng-model="username"]').focus();});
        $scope.login = function () {
            Auth.login({
                username: $scope.username,
                password: $scope.password,
                rememberMe: $scope.rememberMe
            }).then(function () {
                $scope.authenticationError = false;
                var promise = Principal.identity();
                promise.then(function(){
                    if(Principal.isAuthenticated() && Principal.isIdentityResolved() && Principal.isInAnyRole(['CUSTOMER', 'ENGINEER'])) {
                        if(Principal.isInRole('CUSTOMER'))
                            $state.go('balance', $rootScope.previousStateParams);
                        else
                            $state.go('engineer', $rootScope.previousStateParams);
                    }
                    else
                        $rootScope.back();
                }).catch(function(){
                    $rootScope.back();
                });
            }).catch(function () {
                $scope.authenticationError = true;
            });
        };
    });
