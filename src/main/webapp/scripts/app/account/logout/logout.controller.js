'use strict';

angular.module('domofonApp')
    .controller('LogoutController', function (Auth) {
        Auth.logout();
    });
