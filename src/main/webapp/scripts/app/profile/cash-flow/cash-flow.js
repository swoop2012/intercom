'use strict';

angular.module('domofonApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('cash-flow', {
                parent: 'profile.customer',
                url: '/customer/cash-flow',
                data: {
                    roles: ['CUSTOMER']
                },
                views: {
                    'content@profile': {
                        templateUrl: 'scripts/app/profile/cash-flow/cash-flow.html',
                        controller: 'CashFlowController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('cash-flow');
                        return $translate.refresh();
                    }]
                }
            })
    });
