'use strict';

angular.module('domofonApp')
    .controller('CashFlowController', function ($scope, Profile, ngTableParams) {
        var PAGE_SIZE = 2;
        $scope.total = {
            from:new Date(),
            to:new Date()
        };
        Profile.cashFlowTotal(function(data){
            $scope.total = data;
            $scope.balance = data.balance;
        });

        $scope.balance = 0;
        $scope.balanceOffset = 0;
        $scope.cashFlowTableParams = new ngTableParams({
            page: 1,            // show first page
            count: PAGE_SIZE,   // count per page
            sorting: {date: 'desc'}
        }, {
            total: 0,   // length of data
            counts: [], // hide page counts control
            getData: function ($defer, params) {
                var keys = Object.keys(params.$params.sorting);
                var object = {
                    page: params.$params.page - 1,
                    sort: keys[0] + "," + params.$params.sorting[keys[0]]
                };
                object.size = PAGE_SIZE;
                // Ajax call to update the table contents
                Profile.cashFlow(object, function (data) {
                    var balance = $scope.balance - data.balanceOffset;
                    var pageLength = data.page.content.length;
                    for(var i = 0; i < pageLength; i++)
                    {
                        data.page.content[i].balance = balance;
                        if(data.page.content[i].operation == "EXPENSE")
                            balance += data.page.content[i].sum;
                        if(data.page.content[i].operation == "INCOME")
                            balance -= data.page.content[i].sum;
                    }
                    if (pageLength == 0 && data.page.number > 0) {
                        params.page(data.page.number);
                    }
                    else {
                        params.page(data.page.number + 1);
                        params.total(data.page.totalElements);
                        $defer.resolve(data.page.content);
                    }
                });
            }
        });
        $scope.refresh = function () {
            $scope.cashFlowTableParams.reload();
        };

    });
