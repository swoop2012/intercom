'use strict';

angular.module('domofonApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('fee-history', {
                parent: 'profile.customer',
                url: '/customer/fee-history',
                data: {
                    roles: ['CUSTOMER']
                },
                views: {
                    'content@profile': {
                        templateUrl: 'scripts/app/profile/fee-history/fee-history.html',
                        controller: 'FeeHistoryController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('fee-history');
                        return $translate.refresh();
                    }]
                }
            })
    });
