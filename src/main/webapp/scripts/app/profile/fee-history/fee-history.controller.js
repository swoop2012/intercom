'use strict';

angular.module('domofonApp')
    .controller('FeeHistoryController', function ($scope, Profile, ngTableParams) {
        var PAGE_SIZE = 20;
        $scope.feeHistoryTableParams = new ngTableParams({
            page: 1,            // show first page
            count: PAGE_SIZE,   // count per page
            sorting: {date: 'desc'}
        }, {
            total: 0,   // length of data
            counts: [], // hide page counts control
            getData: function ($defer, params) {
                var keys = Object.keys(params.$params.sorting);
                var object = {
                    page: params.$params.page - 1,
                    sort: keys[0] + "," + params.$params.sorting[keys[0]]
                };
                object.size = PAGE_SIZE;
                // Ajax call to update the table contents
                Profile.feeHistory(object, function (data) {
                    if (data.content.length == 0 && data.number > 0) {
                        params.page(data.number);
                    }
                    else {
                        params.page(data.number + 1);
                        params.total(data.totalElements);
                        $defer.resolve(data.content);
                    }
                });
            }
        });
        $scope.refresh = function () {
            $scope.feeHistoryTableParams.reload();
        };
    });
