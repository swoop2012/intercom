'use strict';

angular.module('domofonApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('balance', {
                parent: 'profile.customer',
                url: '/customer/balance',
                data: {
                    roles: ['CUSTOMER']
                },
                views: {
                    'content@profile': {
                        templateUrl: 'scripts/app/profile/balance/balance.html',
                        controller: 'BalanceController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('balance');
                        $translatePartialLoader.addPart('global');
                        $translatePartialLoader.addPart('exempt');
                        return $translate.refresh();
                    }]
                }
            })
    });
