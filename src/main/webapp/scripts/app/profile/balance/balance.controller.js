'use strict';

angular.module('domofonApp')
    .controller('BalanceController', function ($scope, Profile) {
        Profile.balance(function(data) {
            $scope.customer = data.customer;
            $scope.lastFee =  data.lastFee;
            $scope.phones = data.phones;
        });
        $scope.currentDate = new Date();
        $scope.print = function()
        {
            alert("Printing...");
        }
    });
