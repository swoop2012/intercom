'use strict';

angular.module('domofonApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('profile', {
                abstract: true,
                views: {
                    '@': {
                        templateUrl: 'scripts/app/layouts/profile.html'
                    }
                }
            });
        $stateProvider
            .state('profile.customer', {
                abstract: true,
                views: {
                    'navbar@profile': {
                        templateUrl: 'scripts/components/customernavbar/navbar.html',
                        controller: 'CustomerNavbarController'
                    }
                },
                authorize: ['Auth',
                    function (Auth) {
                        return Auth.authorize();
                    }
                ]
            });
    });
