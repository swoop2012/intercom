'use strict';

angular.module('domofonApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('other-payment-methods', {
                parent: 'profile.customer',
                url: '/customer/other-payment-methods',
                data: {
                    roles: ['CUSTOMER']
                },
                views: {
                    'content@profile': {
                        templateUrl: 'scripts/app/profile/other-payment-methods/other-payment-methods.html',
                        controller: 'OtherPaymentMethodsController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('other-payment-methods');
                        return $translate.refresh();
                    }]
                }
            })
    });
