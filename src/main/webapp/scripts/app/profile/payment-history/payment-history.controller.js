'use strict';

angular.module('domofonApp')
    .controller('PaymentHistoryController', function ($scope, Profile, ngTableParams) {
        var PAGE_SIZE = 20;
        $scope.paymentHistoryTableParams = new ngTableParams({
            page: 1,            // show first page
            count: PAGE_SIZE,   // count per page
            sorting: {id: 'desc'}
        }, {
            total: 0,   // length of data
            counts: [], // hide page counts control
            getData: function ($defer, params) {
                var keys = Object.keys(params.$params.sorting);
                var object = {
                    page: params.$params.page - 1,
                    sort: keys[0] + "," + params.$params.sorting[keys[0]]
                };
                object.size = PAGE_SIZE;
                // Ajax call to update the table contents
                Profile.paymentHistory(object, function (data) {
                    $scope.customer = data.customer;
                    if (data.page.content.length == 0 && data.page.number > 0) {
                        params.page(data.page.number);
                    }
                    else {
                        params.page(data.page.number + 1);
                        params.total(data.page.totalElements);
                        $defer.resolve(data.page.content);
                    }
                });
            }
        });
        $scope.refresh = function () {
            $scope.paymentHistoryTableParams.reload();
        };

    });
