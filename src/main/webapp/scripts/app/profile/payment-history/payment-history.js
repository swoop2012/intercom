'use strict';

angular.module('domofonApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('payment-history', {
                parent: 'profile.customer',
                url: '/customer/payment-history',
                data: {
                    roles: ['CUSTOMER']
                },
                views: {
                    'content@profile': {
                        templateUrl: 'scripts/app/profile/payment-history/payment-history.html',
                        controller: 'PaymentHistoryController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('payment-history');
                        $translatePartialLoader.addPart('payment');
                        return $translate.refresh();
                    }]
                }
            })
    });
