'use strict';

angular.module('domofonApp')
    .controller('ExemptController', function ($scope, Exempt, ngTableParams) {
        var PAGE_SIZE = 2;
        $scope.types = Exempt.getTypes();
        $scope.filter = {};
        $scope.exemptTableParams = new ngTableParams({
            page: 1,            // show first page
            count: PAGE_SIZE,   // count per page
            sorting: {id: 'asc'}
        }, {
            total: 0,   // length of data
            counts: [], // hide page counts control
            getData: function ($defer, params) {
                var keys = Object.keys(params.$params.sorting);
                var object = {
                    page: params.$params.page - 1,
                    sort: keys[0] + "," + params.$params.sorting[keys[0]]
                };
                // Filtering present on the page
                var filters = Object.keys($scope.filter);
                for (var i = 0; i < filters.length; i++) {
                    object[filters[i]] = $scope.filter[filters[i]];
                }
                object.size = PAGE_SIZE;
                // Ajax call to update the table contents
                Exempt.query(object, function (data) {
                    if (data.content.length == 0 && data.number > 0) {
                        params.page(data.number);
                    }
                    else {
                        params.page(data.number + 1);
                        params.total(data.totalElements);
                        $defer.resolve(data.content);
                    }
                });
            }
        });
        $scope.refresh = function () {
            $scope.exemptTableParams.reload();
        };

        $scope.refresh();
        $scope.create = function () {
            Exempt.save($scope.exempt,
                function () {
                    $scope.refresh();
                    $('#saveExemptModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.exempt = Exempt.get({id: id});
            $('#saveExemptModal').modal('show');
        };

        $scope.delete = function (id) {
            $scope.exempt = Exempt.get({id: id});
            $('#deleteExemptConfirmation').modal('show');
        };

        $scope.confirmDelete = function (id) {
            Exempt.delete({id: id},
                function () {
                    $scope.refresh();
                    $('#deleteExemptConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.clear = function () {
            $scope.exempt = {extCode: null, type: null, startDate: null, endDate: null, id: null};
            $scope.exemptForm.$setPristine();
        };
    });
