'use strict';

angular.module('domofonApp')
    .controller('ExemptDetailController', function ($scope, $stateParams, Exempt, Customer) {
        $scope.exempt = {};
        $scope.load = function (id) {
            Exempt.get({id: id}, function(result) {
              $scope.exempt = result;
            });
        };
        $scope.load($stateParams.id);
    });
