'use strict';

angular.module('domofonApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('exempt', {
                parent: 'entity',
                url: '/exempt',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/exempt/exempts.html',
                        controller: 'ExemptController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('exempt');
                        return $translate.refresh();
                    }]
                }
            })
            .state('exemptDetail', {
                parent: 'entity',
                url: '/exempt/:id',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/exempt/exempt-detail.html',
                        controller: 'ExemptDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('exempt');
                        return $translate.refresh();
                    }]
                }
            });
    });
