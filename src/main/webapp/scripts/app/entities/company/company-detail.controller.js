'use strict';

angular.module('domofonApp')
    .controller('CompanyDetailController', function ($scope, $stateParams, Company, Affiliate) {
        $scope.company = {};
        $scope.load = function (id) {
            Company.get({id: id}, function(result) {
              $scope.company = result;
            });
        };
        $scope.load($stateParams.id);
    });
