'use strict';

angular.module('domofonApp')
    .controller('CompanyController', function ($scope, Company, Affiliate, ngTableParams) {
        $scope.companies = [];
        $scope.affiliates = Affiliate.loadAll();
        $scope.filter = {};

        var PAGE_SIZE = 5;

        $scope.namePattern = "^[A-Za-zА-Яа-я0-9\\- ]*$";
        $scope.extCodePattern = "^[A-Za-z0-9]*$";

        $scope.companyTableParams = new ngTableParams({
            page: 1,            // show first page
            count: PAGE_SIZE,   // count per page
            sorting: {id: 'asc'}
        }, {
            total: 0,   // length of data
            counts: [], // hide page counts control
            getData: function ($defer, params) {
                var keys = Object.keys(params.$params.sorting);
                var object = {
                    page: params.$params.page - 1,
                    sort: keys[0] + "," + params.$params.sorting[keys[0]]
                };
                // Filtering present on the page
                var filters = Object.keys($scope.filter);
                for (var i = 0; i < filters.length; i++) {
                    object[filters[i]] = $scope.filter[filters[i]];
                }
                object.size = PAGE_SIZE;
                // Ajax call to update the table contents
                Company.query(object, function (data) {
                    if (data.content.length == 0 && data.number > 0) {
                        params.page(data.number);
                    }
                    else {
                        params.page(data.number + 1);
                        params.total(data.totalElements);
                        $defer.resolve(data.content);
                    }
                });
            }
        });
        $scope.refresh = function () {
            $scope.companyTableParams.reload();
        };
        $scope.refresh();

        $scope.create = function () {
            Company.save($scope.company,
                function () {
                    $scope.refresh();
                    $('#saveCompanyModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.company = Company.get({id: id});
            $('#saveCompanyModal').modal('show');
        };

        $scope.delete = function (id) {
            $scope.company = Company.get({id: id});
            $('#deleteCompanyConfirmation').modal('show');
        };

        $scope.confirmDelete = function (id) {
            Company.delete({id: id},
                function () {
                    $scope.refresh();
                    $('#deleteCompanyConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.clear = function () {
            $scope.company = {extCode: null, name: null, id: null};
            $scope.companyForm.$setPristine();
        };
    });
