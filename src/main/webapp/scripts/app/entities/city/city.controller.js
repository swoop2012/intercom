'use strict';

angular.module('domofonApp')
    .controller('CityController', function ($scope, City, Affiliate, ngTableParams) {
        $scope.cities = [];
        $scope.affiliates = Affiliate.loadAll();
        $scope.filter = {};

        var PAGE_SIZE = 5;

        $scope.cityTableParams = new ngTableParams({
            page: 1,            // show first page
            count: PAGE_SIZE,   // count per page
            sorting: {name: 'asc'}
        }, {
            total: 0,   // length of data
            counts: [], // hide page counts control
            getData: function ($defer, params) {
                var keys = Object.keys(params.$params.sorting);
                var object = {
                    page: params.$params.page - 1,
                    sort: keys[0] + "," + params.$params.sorting[keys[0]]
                };
                // Filtering present on the page
                var filters = Object.keys($scope.filter);
                for (var i = 0; i < filters.length; i++) {
                    object[filters[i]] = $scope.filter[filters[i]];
                }
                object.size = PAGE_SIZE;
                // Ajax call to update the table contents
                City.query(object, function (data) {
                    if (data.content.length == 0 && data.number > 0) {
                        params.page(data.number);
                    }
                    else {
                        params.page(data.number + 1);
                        params.total(data.totalElements);
                        $defer.resolve(data.content);
                    }
                });
            }
        });
        $scope.refresh = function () {
            $scope.cityTableParams.reload();
        };

        $scope.refresh();

        $scope.create = function () {
            City.save($scope.city,
                function () {
                    $scope.refresh();
                    $('#saveCityModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.city = City.get({id: id});
            $('#saveCityModal').modal('show');
        };

        $scope.delete = function (id) {
            $scope.city = City.get({id: id});
            $('#deleteCityConfirmation').modal('show');
        };

        $scope.confirmDelete = function (id) {
            City.delete({id: id},
                function () {
                    $scope.refresh();
                    $('#deleteCityConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.clear = function () {
            $scope.city = {extCode: null, name: null, id: null};
            $scope.cityForm.$setPristine();
        };
    });
