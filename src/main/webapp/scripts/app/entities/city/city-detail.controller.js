'use strict';

angular.module('domofonApp')
    .controller('CityDetailController', function ($scope, $stateParams, City, Affiliate) {
        $scope.city = {};
        $scope.load = function (id) {
            City.get({id: id}, function(result) {
              $scope.city = result;
            });
        };
        $scope.load($stateParams.id);
    });
