'use strict';

angular.module('domofonApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('city', {
                parent: 'entity',
                url: '/city',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/city/cities.html',
                        controller: 'CityController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('city');
                        return $translate.refresh();
                    }]
                }
            })
            .state('cityDetail', {
                parent: 'entity',
                url: '/city/:id',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/city/city-detail.html',
                        controller: 'CityDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('city');
                        return $translate.refresh();
                    }]
                }
            });
    });
