'use strict';

angular.module('domofonApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('payment', {
                parent: 'entity',
                url: '/payment',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/payment/payments.html',
                        controller: 'PaymentController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('payment');
                        $translatePartialLoader.addPart('service');
                        return $translate.refresh();
                    }]
                }
            })
            .state('paymentDetail', {
                parent: 'entity',
                url: '/payment/:id',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/payment/payment-detail.html',
                        controller: 'PaymentDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('payment');
                        return $translate.refresh();
                    }]
                }
            });
    });
