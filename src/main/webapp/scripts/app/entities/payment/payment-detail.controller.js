'use strict';

angular.module('domofonApp')
    .controller('PaymentDetailController', function ($scope, $stateParams, Payment, Customer, Service) {
        $scope.payment = {};
        $scope.load = function (id) {
            Payment.get({id: id}, function(result) {
              $scope.payment = result;
            });
        };
        $scope.load($stateParams.id);
    });
