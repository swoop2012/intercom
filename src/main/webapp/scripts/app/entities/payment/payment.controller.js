'use strict';

angular.module('domofonApp')
    .controller('PaymentController', function ($scope, Payment, Customer, ngTableParams) {
        $scope.filter = {};
        $scope.customers = [];
        $scope.types = Payment.getTypes();
        $scope.services = Payment.getServices();
        $scope.results = Payment.getResults();
        $scope.operations = Payment.getOperations();
        var PAGE_SIZE = 2;
        $scope.paymentTableParams = new ngTableParams({
            page: 1,            // show first page
            count: PAGE_SIZE,   // count per page
            sorting: {date: 'desc'}
        }, {
            total: 0,   // length of data
            counts: [], // hide page counts control
            getData: function ($defer, params) {
                var keys = Object.keys(params.$params.sorting);
                var object = {
                    page: params.$params.page - 1,
                    sort: keys[0] + "," + params.$params.sorting[keys[0]]
                };
                // Filtering present on the page
                var filters = Object.keys($scope.filter);
                for (var i = 0; i < filters.length; i++) {
                    object[filters[i]] = $scope.filter[filters[i]];
                }
                object.size = PAGE_SIZE;
                // Ajax call to update the table contents
                Payment.query(object, function (data) {
                    if (data.content.length == 0 && data.number > 0) {
                        params.page(data.number);
                    }
                    else {
                        params.page(data.number + 1);
                        params.total(data.totalElements);
                        $defer.resolve(data.content);
                    }
                });
            }
        });
        $scope.refresh = function () {
            $scope.paymentTableParams.reload();
        };

        $scope.refreshCustomers = function(search){
            if($.trim(search) != "")
                Customer.getOptions({user:search}, function(result) {
                    $scope.customers = result;
                })
        };

        $scope.refresh();

        $scope.create = function () {
            Payment.save($scope.payment,
                function () {
                    $scope.refresh();
                    $('#savePaymentModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.payment = Payment.get({id: id});
            $('#savePaymentModal').modal('show');
        };

        $scope.delete = function (id) {
            $scope.payment = Payment.get({id: id});
            $('#deletePaymentConfirmation').modal('show');
        };

        $scope.confirmDelete = function (id) {
            Payment.delete({id: id},
                function () {
                    $scope.refresh();
                    $('#deletePaymentConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.clear = function () {
            $scope.payment = {extCode: null, date: null, document: null, sum: null, operation: null, type: null, id: null};
            $scope.paymentForm.$setPristine();
        };
    });
