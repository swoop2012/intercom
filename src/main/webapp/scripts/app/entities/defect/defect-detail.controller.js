'use strict';

angular.module('domofonApp')
    .controller('DefectDetailController', function ($scope, $stateParams, Defect) {
        $scope.defect = {};
        $scope.load = function (id) {
            Defect.get({id: id}, function(result) {
              $scope.defect = result;
            });
        };
        $scope.load($stateParams.id);
    });
