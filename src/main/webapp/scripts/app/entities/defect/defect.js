'use strict';

angular.module('domofonApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('defect', {
                parent: 'entity',
                url: '/defect',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/defect/defects.html',
                        controller: 'DefectController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('defect');
                        return $translate.refresh();
                    }]
                }
            })
            .state('defectDetail', {
                parent: 'entity',
                url: '/defect/:id',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/defect/defect-detail.html',
                        controller: 'DefectDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('defect');
                        return $translate.refresh();
                    }]
                }
            });
    });
