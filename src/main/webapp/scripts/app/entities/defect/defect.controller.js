'use strict';

angular.module('domofonApp')
    .controller('DefectController', function ($scope, Defect) {
        $scope.defects = [];
        $scope.loadAll = function() {
            Defect.query(function(result) {
               $scope.defects = result;
            });
        };
        $scope.loadAll();

        $scope.create = function () {
            Defect.save($scope.defect,
                function () {
                    $scope.loadAll();
                    $('#saveDefectModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.defect = Defect.get({id: id});
            $('#saveDefectModal').modal('show');
        };

        $scope.delete = function (id) {
            $scope.defect = Defect.get({id: id});
            $('#deleteDefectConfirmation').modal('show');
        };

        $scope.confirmDelete = function (id) {
            Defect.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteDefectConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.clear = function () {
            $scope.defect = {extCode: null, name: null, id: null};
        };
    });
