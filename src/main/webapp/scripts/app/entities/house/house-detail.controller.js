'use strict';

angular.module('domofonApp')
    .controller('HouseDetailController', function ($scope, $stateParams, House, Street) {
        $scope.house = {};
        $scope.load = function (id) {
            House.get({id: id}, function(result) {
              $scope.house = result;
            });
        };
        $scope.load($stateParams.id);
    });
