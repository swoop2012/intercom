'use strict';

angular.module('domofonApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('house', {
                parent: 'entity',
                url: '/house',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/house/houses.html',
                        controller: 'HouseController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('house');
                        return $translate.refresh();
                    }]
                }
            })
            .state('houseDetail', {
                parent: 'entity',
                url: '/house/:id',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/house/house-detail.html',
                        controller: 'HouseDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('house');
                        return $translate.refresh();
                    }]
                }
            });
    });
