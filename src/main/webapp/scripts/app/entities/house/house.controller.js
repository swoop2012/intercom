'use strict';

angular.module('domofonApp')
    .controller('HouseController', function ($scope, House, Street, City, ngTableParams) {
        $scope.houses = [];
        $scope.streets = [];
        $scope.cities = [];
        $scope.city = {};
        $scope.filter = {};
        $scope.namePattern = "^[А-Яа-яA-Za-z0-9,/\\-]*$";
        $scope.extCodePattern = "^[A-Za-z0-9]*$";

        var PAGE_SIZE = 20;

        $scope.houseTableParams = new ngTableParams({
            page: 1,            // show first page
            count: PAGE_SIZE,   // count per page
            sorting: {id: 'asc'}
        }, {
            total: 0,   // length of data
            counts: [], // hide page counts control
            getData: function ($defer, params) {
                var keys = Object.keys(params.$params.sorting);
                var object = {
                    page: params.$params.page - 1,
                    sort: keys[0] + "," + params.$params.sorting[keys[0]]
                };
                // Filtering present on the page
                var filters = Object.keys($scope.filter);
                for (var i = 0; i < filters.length; i++) {
                    object[filters[i]] = $scope.filter[filters[i]];
                }
                object.size = PAGE_SIZE;
                // Ajax call to update the table contents
                House.query(object, function (data) {
                    if (data.content.length == 0 && data.number > 0) {
                        params.page(data.number);
                    }
                    else {
                        params.page(data.number + 1);
                        params.total(data.totalElements);
                        $defer.resolve(data.content);
                    }
                });
            }
        });

        $scope.refresh = function () {
            $scope.houseTableParams.reload();
        };

        $scope.refresh();

        $scope.refreshStreets = function(search){
            if($.trim(search) != "")
                Street.query({name:search, cityId: $scope.city.id}, function(result) {
                    $scope.streets = result.hasOwnProperty("content") ? result.content : result;
                })
        };

        $scope.refreshCities = function(search) {
            if($.trim(search) != "")
                City.query({name:search}, function(result) {
                    $scope.cities = result.hasOwnProperty('content') ? result.content : result;
                })
        };

        $scope.create = function () {
            if ($scope.house.inaccurateAddress == null)
                $scope.house.inaccurateAddress = false;

            if ($scope.house.scanned == null)
                $scope.house.scanned = false;

            House.save($scope.house,
                function () {
                    $scope.refresh();
                    $('#saveHouseModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.house = House.get({id: id});
            $('#saveHouseModal').modal('show');
        };

        $scope.delete = function (id) {
            $scope.house = House.get({id: id});
            $('#deleteHouseConfirmation').modal('show');
        };

        $scope.confirmDelete = function (id) {
            House.delete({id: id},
                function () {
                    $scope.refresh();
                    $('#deleteHouseConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.clear = function () {
            $scope.house = {extCode: null, name: null, latitude: null, longitude: null, inaccurateAddress: null, scanned: null, id: null};
            $scope.cities = [];
            $scope.streets = [];
            $scope.city = {};
            $scope.houseForm.$setPristine();
        };
    });
