'use strict';

angular.module('domofonApp')
    .controller('MediaController', function ($scope, Media) {
        $scope.medias = [];
        $scope.loadAll = function() {
            Media.query(function(result) {
               $scope.medias = result;
            });
        };
        $scope.loadAll();

        $scope.create = function () {
            Media.save($scope.media,
                function () {
                    $scope.loadAll();
                    $('#saveMediaModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.media = Media.get({id: id});
            $('#saveMediaModal').modal('show');
        };

        $scope.delete = function (id) {
            $scope.media = Media.get({id: id});
            $('#deleteMediaConfirmation').modal('show');
        };

        $scope.confirmDelete = function (id) {
            Media.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteMediaConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.clear = function () {
            $scope.media = {type: null, fileName: null, id: null};
        };
    });
