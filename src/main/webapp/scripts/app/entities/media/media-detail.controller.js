'use strict';

angular.module('domofonApp')
    .controller('MediaDetailController', function ($scope, $stateParams, Media) {
        $scope.media = {};
        $scope.load = function (id) {
            Media.get({id: id}, function(result) {
              $scope.media = result;
            });
        };
        $scope.load($stateParams.id);
    });
