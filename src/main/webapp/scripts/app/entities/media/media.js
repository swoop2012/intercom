'use strict';

angular.module('domofonApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('media', {
                parent: 'entity',
                url: '/media',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/media/medias.html',
                        controller: 'MediaController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('media');
                        return $translate.refresh();
                    }]
                }
            })
            .state('mediaDetail', {
                parent: 'entity',
                url: '/media/:id',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/media/media-detail.html',
                        controller: 'MediaDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('media');
                        return $translate.refresh();
                    }]
                }
            });
    });
