'use strict';

angular.module('domofonApp')
    .controller('EntranceController', function ($scope, Entrance, House, Street, City, ngTableParams) {
        $scope.entrances = [];

        // next houses, streets, fstreets, cities - for search and filter
        $scope.houses = [];
        $scope.streets = [];
        $scope.fstreets = []; // streets for filter part
        $scope.cities = [];

        $scope.city = {};
        $scope.street = {};
        $scope.house = {};

        $scope.filter = {};
        $scope.namePattern = "^[0-9]*$";
        $scope.extCodePattern = "^[A-Za-z0-9]*$";
        $scope.domofonCodePattern = "^\\d*$";

        var PAGE_SIZE = 20;

        $scope.entranceTableParams = new ngTableParams({
            page: 1,            // show first page
            count: PAGE_SIZE,   // count per page
            sorting: {id: 'asc'}
        }, {
            total: 0,   // length of data
            counts: [], // hide page counts control
            getData: function ($defer, params) {
                var keys = Object.keys(params.$params.sorting);
                var object = {
                    page: params.$params.page - 1,
                    sort: keys[0] + "," + params.$params.sorting[keys[0]]
                };
                // Filtering present on the page
                var filters = Object.keys($scope.filter);
                for (var i = 0; i < filters.length; i++) {
                    object[filters[i]] = $scope.filter[filters[i]];
                }
                object.size = PAGE_SIZE;
                // Ajax call to update the table contents
                Entrance.query(object, function (data) {
                    if (data.content.length == 0 && data.number > 0) {
                        params.page(data.number);
                    }
                    else {
                        params.page(data.number + 1);
                        params.total(data.totalElements);
                        $defer.resolve(data.content);
                    }
                });
            }
        });

        $scope.refresh = function () {
            $scope.entranceTableParams.reload();
        };

        $scope.refresh();

        $scope.refreshHouses = function(search) {
            if($.trim(search) != "")
                House.query({name:search, cityId: $scope.city.id, streetId: $scope.street.id,
                    inaccurateAddress:false , scanned:false}, function(result)
                {
                    $scope.houses = result.hasOwnProperty('content') ? result.content : result;
                })
        };

        $scope.filterStreets = function(search) {
            if ($.trim(search) != "")
                Street.query({name:search, cityId: $scope.filter.cityId}, function(result) {
                    $scope.fstreets = result.hasOwnProperty('content') ? result.content : result;
                })
        };

        $scope.refreshStreets = function(search) {
            if($.trim(search) != "")
                Street.query({name:search, cityId: $scope.city.id}, function(result) {
                    $scope.streets = result.hasOwnProperty('content') ? result.content : result;
                })
        };

        $scope.refreshCities = function(search) {
            if($.trim(search) != "")
                City.query({name:search}, function(result) {
                    $scope.cities = result.hasOwnProperty('content') ? result.content : result;
                })
        };

        $scope.create = function () {
            if ($scope.entrance.gate == null)
            $scope.entrance.gate = false;

            Entrance.save($scope.entrance,
                function () {
                    $scope.refresh();
                    $('#saveEntranceModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.entrance = Entrance.get({id: id});
            $('#saveEntranceModal').modal('show');
        };

        $scope.delete = function (id) {
            $scope.entrance = Entrance.get({id: id});
            $('#deleteEntranceConfirmation').modal('show');
        };

        $scope.confirmDelete = function (id) {
            Entrance.delete({id: id},
                function () {
                    $scope.refresh();
                    $('#deleteEntranceConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.clear = function () {
            $scope.entrance = {extCode: null, name: null, domofonCode: null, gate: null, id: null};
            $scope.cities = [];
            $scope.streets = [];
            $scope.houses = [];
            $scope.city = {};
            $scope.street = {};
            $scope.house = {};
            $scope.entranceForm.$setPristine();
        };
    });
