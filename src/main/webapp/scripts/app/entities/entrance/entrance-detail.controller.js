'use strict';

angular.module('domofonApp')
    .controller('EntranceDetailController', function ($scope, $stateParams, Entrance, House) {
        $scope.entrance = {};
        $scope.load = function (id) {
            Entrance.get({id: id}, function(result) {
              $scope.entrance = result;
            });
        };
        $scope.load($stateParams.id);
    });
