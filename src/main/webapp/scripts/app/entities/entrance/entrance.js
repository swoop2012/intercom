'use strict';

angular.module('domofonApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('entrance', {
                parent: 'entity',
                url: '/entrance',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/entrance/entrances.html',
                        controller: 'EntranceController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('entrance');
                        return $translate.refresh();
                    }]
                }
            })
            .state('entranceDetail', {
                parent: 'entity',
                url: '/entrance/:id',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/entrance/entrance-detail.html',
                        controller: 'EntranceDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('entrance');
                        return $translate.refresh();
                    }]
                }
            });
    });
