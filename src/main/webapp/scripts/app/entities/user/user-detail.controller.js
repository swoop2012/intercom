'use strict';

angular.module('domofonApp')
    .controller('UserDetailController', function ($scope, $stateParams, User, Affiliate) {
        $scope.user = {};
        $scope.load = function (id) {
            User.get({id: id}, function(result) {
              $scope.user = result;
            });
        };
        $scope.load($stateParams.id);
    });
