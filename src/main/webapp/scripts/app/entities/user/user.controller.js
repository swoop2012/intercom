'use strict';

angular.module('domofonApp')
    .controller('UserController', function ($scope, User, Affiliate, ngTableParams) {
        $scope.users = [];
        $scope.filter = {};
        $scope.affiliates = Affiliate.loadAll();
        $scope.roles = User.getRoles();
        var PAGE_SIZE = 2;
        $scope.userTableParams = new ngTableParams({
            page: 1,            // show first page
            count: PAGE_SIZE,   // count per page
            sorting: {fio: 'asc'}
        }, {
            total: 0,   // length of data
            counts: [], // hide page counts control
            getData: function ($defer, params) {
                var keys = Object.keys(params.$params.sorting);
                var object = {
                    page: params.$params.page - 1,
                    sort: keys[0] + "," + params.$params.sorting[keys[0]]
                };
                // Filtering present on the page
                var filters = Object.keys($scope.filter);
                for (var i = 0; i < filters.length; i++) {
                    object[filters[i]] = $scope.filter[filters[i]];
                }
                object.size = PAGE_SIZE;
                // Ajax call to update the table contents
                User.query(object, function (data) {
                    if (data.content.length == 0 && data.number > 0) {
                        params.page(data.number);
                    }
                    else {
                        params.page(data.number + 1);
                        params.total(data.totalElements);
                        $defer.resolve(data.content);
                    }
                });
            }
        });
        $scope.refresh = function () {
            $scope.userTableParams.reload();
        };

        $scope.refresh();

        $scope.create = function () {
            User.save($scope.user,
                function () {
                    $scope.refresh();
                    $('#saveUserModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.user = User.get({id: id});
            $('#saveUserModal').modal('show');
        };

        $scope.delete = function (id) {
            $scope.user = User.get({id: id});
            $('#deleteUserConfirmation').modal('show');
        };

        $scope.confirmDelete = function (id) {
            User.delete({id: id},
                function () {
                    $scope.refresh();
                    $('#deleteUserConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.clear = function () {
            $scope.user = {extCode: null, fio: null, login: null, password: null, id: null,affiliate: null};
            $scope.userForm.$setPristine();
        };
    });
