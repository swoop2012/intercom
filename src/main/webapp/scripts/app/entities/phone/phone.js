'use strict';

angular.module('domofonApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('phone', {
                parent: 'entity',
                url: '/phone',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/phone/phones.html',
                        controller: 'PhoneController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('phone');
                        return $translate.refresh();
                    }]
                }
            })
            .state('phoneDetail', {
                parent: 'entity',
                url: '/phone/:id',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/phone/phone-detail.html',
                        controller: 'PhoneDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('phone');
                        return $translate.refresh();
                    }]
                }
            });
    });
