'use strict';

angular.module('domofonApp')
    .controller('PhoneDetailController', function ($scope, $stateParams, Phone, Customer) {
        $scope.phone = {};
        $scope.load = function (id) {
            Phone.get({id: id}, function(result) {
              $scope.phone = result;
            });
        };
        $scope.load($stateParams.id);
    });
