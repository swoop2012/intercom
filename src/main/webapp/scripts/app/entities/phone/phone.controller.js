'use strict';

angular.module('domofonApp')
    .controller('PhoneController', function ($scope, Phone, Customer, ngTableParams) {
        $scope.filter = {};
        $scope.customers = [];
        var PAGE_SIZE = 2;
        $scope.phoneTableParams = new ngTableParams({
            page: 1,            // show first page
            count: PAGE_SIZE,   // count per page
            sorting: {customer: 'asc'}
        }, {
            total: 0,   // length of data
            counts: [], // hide page counts control
            getData: function ($defer, params) {
                var keys = Object.keys(params.$params.sorting);
                var object = {
                    page: params.$params.page - 1,
                    sort: keys[0] + "," + params.$params.sorting[keys[0]]
                };
                // Filtering present on the page
                var filters = Object.keys($scope.filter);
                for (var i = 0; i < filters.length; i++) {
                    object[filters[i]] = $scope.filter[filters[i]];
                }
                object.size = PAGE_SIZE;
                // Ajax call to update the table contents
                Phone.query(object, function (data) {
                    if (data.content.length == 0 && data.number > 0) {
                        params.page(data.number);
                    }
                    else {
                        params.page(data.number + 1);
                        params.total(data.totalElements);
                        $defer.resolve(data.content);
                    }
                });
            }
        });
        $scope.refresh = function () {
            $scope.phoneTableParams.reload();
        };

        $scope.refresh();

        $scope.refreshCustomers = function(search){
            if($.trim(search) != "")
                Customer.getOptions({user:search}, function(result) {
                    $scope.customers = result;
                })
        };

        $scope.create = function () {
            Phone.save($scope.phone,
                function () {
                    $scope.refresh();
                    $('#savePhoneModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.phone = Phone.get({id: id});
            $('#savePhoneModal').modal('show');
        };

        $scope.delete = function (id) {
            $scope.phone = Phone.get({id: id});
            $('#deletePhoneConfirmation').modal('show');
        };

        $scope.confirmDelete = function (id) {
            Phone.delete({id: id},
                function () {
                    $scope.refresh();
                    $('#deletePhoneConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.clear = function () {
            $scope.phone = {extCode: null, number: null, id: null};
            $scope.phoneForm.$setPristine();
        };
    });
