'use strict';

angular.module('domofonApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('engineer', {
                parent: 'entity',
                url: '/engineer',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/engineer/engineers.html',
                        controller: 'EngineerController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('engineer');
                        return $translate.refresh();
                    }]
                }
            })
            .state('engineerDetail', {
                parent: 'entity',
                url: '/engineer/:id',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/engineer/engineer-detail.html',
                        controller: 'EngineerDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('engineer');
                        return $translate.refresh();
                    }]
                }
            });
    });
