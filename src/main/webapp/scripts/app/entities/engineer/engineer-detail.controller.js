'use strict';

angular.module('domofonApp')
    .controller('EngineerDetailController', function ($scope, $stateParams, Engineer, User, Phone) {
        $scope.engineer = {};
        $scope.load = function (id) {
            Engineer.get({id: id}, function(result) {
              $scope.engineer = result;
            });
        };
        $scope.load($stateParams.id);
    });
