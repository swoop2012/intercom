'use strict';

angular.module('domofonApp')
    .controller('EngineerController', function ($scope, Engineer, User, Phone, ngTableParams) {
        $scope.engineers = [];
        $scope.filter = {};
        $scope.users = [];
        var PAGE_SIZE = 5;

        $scope.phonePattern = "^[0-9()\\- ]*$";
        $scope.extCodePattern = "^[A-Za-z0-9]*$";

        $scope.engineerTableParams = new ngTableParams({
            page: 1,            // show first page
            count: PAGE_SIZE,   // count per page
            sorting: {user: 'asc'}
        }, {
            total: 0,   // length of data
            counts: [], // hide page counts control
            getData: function ($defer, params) {
                var keys = Object.keys(params.$params.sorting);
                var object = {
                    page: params.$params.page - 1,
                    sort: keys[0] + "," + params.$params.sorting[keys[0]]
                };
                // Filtering present on the page
                var filters = Object.keys($scope.filter);
                for (var i = 0; i < filters.length; i++) {
                    object[filters[i]] = $scope.filter[filters[i]];
                }
                object.size = PAGE_SIZE;
                // Ajax call to update the table contents
                Engineer.query(object, function (data) {
                    if (data.content.length == 0 && data.number > 0) {
                        params.page(data.number);
                    }
                    else {
                        params.page(data.number + 1);
                        params.total(data.totalElements);
                        $defer.resolve(data.content);
                    }
                });
            }
        });
        $scope.refresh = function () {
            $scope.engineerTableParams.reload();
        };
        $scope.refresh();

        $scope.refreshUsers = function(search){
            if($.trim(search) != "")
                User.query({fio:search, role:"ENGINEER"}, function(result) {
                    $scope.users = result['content'];
                })
        };

        $scope.create = function () {
            Engineer.save($scope.engineer,
                function () {
                    $scope.refresh();
                    $('#saveEngineerModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.engineer = Engineer.get({id: id});
            $('#saveEngineerModal').modal('show');
        };

        $scope.delete = function (id) {
            $scope.engineer = Engineer.get({id: id});
            $('#deleteEngineerConfirmation').modal('show');
        };

        $scope.confirmDelete = function (id) {
            Engineer.delete({id: id},
                function () {
                    $scope.refresh();
                    $('#deleteEngineerConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.clear = function () {
            $scope.engineer = {extCode: null, id: null};
            $scope.engineerForm.$setPristine();
        };
    });
