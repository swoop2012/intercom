'use strict';

angular.module('domofonApp')
    .controller('StreetDetailController', function ($scope, $stateParams, Street, City) {
        $scope.street = {};
        $scope.load = function (id) {
            Street.get({id: id}, function(result) {
              $scope.street = result;
            });
        };
        $scope.load($stateParams.id);
    });
