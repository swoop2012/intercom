'use strict';

angular.module('domofonApp')
    .controller('StreetController', function ($scope, Street, City, ngTableParams) {
        $scope.streets = [];
        $scope.cities = [];
        $scope.filter = {};

        $scope.extCodePattern = "^[A-Za-z0-9]*$";
        $scope.namePattern = "^[А-Яа-яA-Za-z0-9, ]*$";

        var PAGE_SIZE = 10;

        $scope.streetTableParams = new ngTableParams({
            page: 1,            // show first page
            count: PAGE_SIZE,   // count per page
            sorting: {name: 'asc'}
        }, {
            total: 0,   // length of data
            counts: [], // hide page counts control
            getData: function ($defer, params) {
                var keys = Object.keys(params.$params.sorting);
                var object = {
                    page: params.$params.page - 1,
                    sort: keys[0] + "," + params.$params.sorting[keys[0]]
                };
                // Filtering present on the page
                var filters = Object.keys($scope.filter);
                for (var i = 0; i < filters.length; i++) {
                    object[filters[i]] = $scope.filter[filters[i]];
                }
                object.size = PAGE_SIZE;
                // Ajax call to update the table contents
                Street.query(object, function (data) {
                    if (data.content.length == 0 && data.number > 0) {
                        params.page(data.number);
                    }
                    else {
                        params.page(data.number + 1);
                        params.total(data.totalElements);
                        $defer.resolve(data.content);
                    }
                });
            }
        });

        $scope.refresh = function () {
            $scope.streetTableParams.reload();
        };

        $scope.refresh();

        $scope.refreshCities = function(search){
            if($.trim(search) != "")
                City.query({name:search}, function(result) {
                    $scope.cities = result.hasOwnProperty('content') ? result.content : result;
                })
        };

        $scope.create = function () {
            Street.save($scope.street,
                function () {
                    $scope.refresh();
                    $('#saveStreetModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.street = Street.get({id: id});
            $('#saveStreetModal').modal('show');
        };

        $scope.delete = function (id) {
            $scope.street = Street.get({id: id});
            $('#deleteStreetConfirmation').modal('show');
        };

        $scope.confirmDelete = function (id) {
            Street.delete({id: id},
                function () {
                    $scope.refresh();
                    $('#deleteStreetConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.clear = function () {
            $scope.street = {extCode: null, name: null, id: null};
            $scope.streetForm.$setPristine();
        };
    });
