'use strict';

angular.module('domofonApp')
    .controller('AffiliateController', function ($scope, Affiliate, ngTableParams, Principal) {
        $scope.filter = {};
        $scope.isAdmin = Principal.isInRole('ADMIN')
        var PAGE_SIZE = 2;
        $scope.affiliateTableParams = new ngTableParams({
            page: 1,            // show first page
            count: PAGE_SIZE,   // count per page
            sorting: {name: 'asc'}
        }, {
            total: 0,   // length of data
            counts: [], // hide page counts control
            getData: function ($defer, params) {
                var keys = Object.keys(params.$params.sorting);
                var object = {
                    page: params.$params.page - 1,
                    sort: keys[0] + "," + params.$params.sorting[keys[0]]
                };
                // Filtering present on the page
                var filters = Object.keys($scope.filter);
                for (var i = 0; i < filters.length; i++) {
                    object[filters[i]] = $scope.filter[filters[i]];
                }
                object.size = PAGE_SIZE;
                // Ajax call to update the table contents
                Affiliate.query(object, function (data) {
                    if (data.content.length == 0 && data.number > 0) {
                        params.page(data.number);
                    }
                    else {
                        params.page(data.number + 1);
                        params.total(data.totalElements);
                        $defer.resolve(data.content);
                    }
                });
            }
        });
        $scope.refresh = function () {
            $scope.affiliateTableParams.reload();
        };

        $scope.refresh();

        $scope.create = function () {
            Affiliate.save($scope.affiliate,
                function () {
                    $scope.refresh();
                    $('#saveAffiliateModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.affiliate = Affiliate.get({id: id});
            $('#saveAffiliateModal').modal('show');
        };

        $scope.delete = function (id) {
            $scope.affiliate = Affiliate.get({id: id});
            $('#deleteAffiliateConfirmation').modal('show');
        };

        $scope.confirmDelete = function (id) {
            Affiliate.delete({id: id},
                function () {
                    $scope.refresh();
                    $('#deleteAffiliateConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.clear = function () {
            $scope.affiliate = {extCode: null, name: null, id: null};
            $scope.affiliateForm.$setPristine();
        };
    });
