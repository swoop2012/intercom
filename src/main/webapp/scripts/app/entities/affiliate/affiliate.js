'use strict';

angular.module('domofonApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('affiliate', {
                parent: 'entity',
                url: '/affiliate',
                data: {
                    roles: ['ADMIN', 'EMPLOYEE']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/affiliate/affiliates.html',
                        controller: 'AffiliateController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('affiliate');
                        return $translate.refresh();
                    }]
                }
            })
            .state('affiliateDetail', {
                parent: 'entity',
                url: '/affiliate/:id',
                data: {
                    roles: ['ADMIN', 'EMPLOYEE']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/affiliate/affiliate-detail.html',
                        controller: 'AffiliateDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('affiliate');
                        return $translate.refresh();
                    }]
                }
            });
    });
