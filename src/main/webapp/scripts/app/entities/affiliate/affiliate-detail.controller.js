'use strict';

angular.module('domofonApp')
    .controller('AffiliateDetailController', function ($scope, $stateParams, Affiliate, User) {
        $scope.affiliate = {};
        $scope.load = function (id) {
            Affiliate.get({id: id}, function(result) {
              $scope.affiliate = result;
            });
        };
        $scope.load($stateParams.id);
    });
