'use strict';

angular.module('domofonApp')
    .controller('ContractDetailController', function ($scope, $stateParams, Contract, Company) {
        $scope.contract = {};
        $scope.load = function (id) {
            Contract.get({id: id}, function(result) {
              $scope.contract = result;
            });
        };
        $scope.load($stateParams.id);
    });
