'use strict';

angular.module('domofonApp')
    .controller('ContractController', function ($scope, Contract, Company, ngTableParams) {
        $scope.contracts = [];
        $scope.companies = [];
        $scope.filter = {};

        $scope.numberPattern = "^[0-9]*$";
        $scope.extCodePattern = "^[A-Za-z0-9]*$";

        var PAGE_SIZE = 10;

        $scope.contractTableParams = new ngTableParams({
            page: 1,            // show first page
            count: PAGE_SIZE,   // count per page
            sorting: {id: 'asc'}
        }, {
            total: 0,   // length of data
            counts: [], // hide page counts control
            getData: function ($defer, params) {
                var keys = Object.keys(params.$params.sorting);
                var object = {
                    page: params.$params.page - 1,
                    sort: keys[0] + "," + params.$params.sorting[keys[0]]
                };
                // Filtering present on the page
                var filters = Object.keys($scope.filter);
                for (var i = 0; i < filters.length; i++) {
                    object[filters[i]] = $scope.filter[filters[i]];
                }
                object.size = PAGE_SIZE;
                // Ajax call to update the table contents
                Contract.query(object, function (data) {
                    if (data.content.length == 0 && data.number > 0) {
                        params.page(data.number);
                    }
                    else {
                        params.page(data.number + 1);
                        params.total(data.totalElements);
                        $defer.resolve(data.content);
                    }
                });
            }
        });

        $scope.refresh = function () {
            $scope.contractTableParams.reload();
        };

        $scope.refresh();

        $scope.refreshCompanies = function(search){
            if($.trim(search) != "")
                Company.query({name:search}, function(result) {
                    $scope.companies = result.hasOwnProperty('content') ? result.content : result;
                })
        };

        $scope.create = function () {
            Contract.save($scope.contract,
                function () {
                    $scope.refresh();
                    $('#saveContractModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.contract = Contract.get({id: id});
            $('#saveContractModal').modal('show');
        };

        $scope.delete = function (id) {
            $scope.contract = Contract.get({id: id});
            $('#deleteContractConfirmation').modal('show');
        };

        $scope.confirmDelete = function (id) {
            Contract.delete({id: id},
                function () {
                    $scope.refresh();
                    $('#deleteContractConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.clear = function () {
            $scope.contract = {extCode: null, number: null, date: null, id: null};
            $scope.contractForm.$setPristine();
        };
    });
