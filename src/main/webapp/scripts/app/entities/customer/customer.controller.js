'use strict';

angular.module('domofonApp')
    .controller('CustomerController', function ($scope, Customer, User, ngTableParams, Exempt, $filter, Contract,
        House, Street, City, Entrance) {
        $scope.customers = [];
        $scope.filter = {};
        $scope.users = [];
        $scope.contracts = [];
        initAddressSelects();
        Exempt.loadAll(function(data){
            angular.forEach(data, function(value){
                value.name = $scope.getFormattedExempt(value);
            });
            $scope.exempts = data;
        });
        $scope.types = Customer.getReceiverTypes();
        $scope.getFormattedExempt = function (exempt){
            if(!exempt)
                return '';
            var str = $filter('translate')('domofonApp.exempt.types.' + exempt.type);
            str += ' ' + $filter('date')(exempt.startDate,'dd MMMM yyyy');
            str += ' - ' + $filter('date')(exempt.endDate,'dd MMMM yyyy');
            return str;
        };

        var PAGE_SIZE = 2;
        $scope.customerTableParams = new ngTableParams({
            page: 1,            // show first page
            count: PAGE_SIZE,   // count per page
            sorting: {connectionDate: 'desc'}
        }, {
            total: 0,   // length of data
            counts: [], // hide page counts control
            getData: function ($defer, params) {
                var keys = Object.keys(params.$params.sorting);
                var object = {
                    page: params.$params.page - 1,
                    sort: keys[0] + "," + params.$params.sorting[keys[0]]
                };
                // Filtering present on the page
                var filters = Object.keys($scope.filter);
                for (var i = 0; i < filters.length; i++) {
                    object[filters[i]] = $scope.filter[filters[i]];
                }
                object.size = PAGE_SIZE;
                // Ajax call to update the table contents
                Customer.query(object, function (data) {
                    if (data.content.length == 0 && data.number > 0) {
                        params.page(data.number);
                    }
                    else {
                        params.page(data.number + 1);
                        params.total(data.totalElements);
                        $defer.resolve(data.content);
                    }
                });
            }
        });
        $scope.refresh = function () {
            $scope.customerTableParams.reload();
        };
        $scope.refresh();
        $scope.refreshUsers = function(search){
            if($.trim(search) != "")
                User.getOptions({fio:search}, function(result) {
                    $scope.users = result;
                })
        };
        $scope.refreshContracts = function(search){
            if($.trim(search) != "")
                Contract.query({number:search, size:10}, function(result) {
                    $scope.contracts = result.content;
                })
        };

        $scope.refreshCities = function(search, filter) {
            var field = getListName('cities', filter);
            if($.trim(search) != "")
                City.query({name:search}, function(result) {
                    $scope[field] = result.hasOwnProperty('content') ? result.content : result;
                    $scope[getListName('streets', filter)] = [];
                    $scope[getListName('houses', filter)] = [];
                    $scope[getListName('entrances', filter)] = [];
                })
        };

        $scope.refreshStreets = function(search, cityId, filter) {
            var field = getListName('streets', filter);
            if($.trim(search) != "")
                Street.query({name:search, cityId: cityId}, function(result) {
                    $scope[field] = result.content;
                    $scope[getListName('houses', filter)] = [];
                    $scope[getListName('entrances', filter)] = [];
                })
        };

        $scope.refreshHouses = function(search, streetId, filter) {
            var field = getListName('houses', filter);
            House.query({name:search, streetId: streetId,
                inaccurateAddress:false , scanned:false}, function(result)
            {
                $scope[field] = result.content;
                $scope[getListName('entrances', filter)] = [];
            })
        };

        $scope.refreshEntrances = function(search, houseId, filter) {
            var field = getListName('entrances', filter);
            if($.trim(search) != "")
                Entrance.query({name:search, houseId: houseId}, function(result)
                {
                    $scope[field] = result.hasOwnProperty('content') ? result.content : result;
                })
        };



        $scope.create = function () {
            Customer.save($scope.customer,
                function () {
                    $scope.refresh();
                    $('#saveCustomerModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.customer = Customer.get({id: id});
            $('#saveCustomerModal').modal('show');
        };

        $scope.delete = function (id) {
            $scope.customer = Customer.get({id: id});
            $('#deleteCustomerConfirmation').modal('show');
        };

        $scope.confirmDelete = function (id) {
            Customer.delete({id: id},
                function () {
                    $scope.refresh();
                    $('#deleteCustomerConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        function initAddressSelects() {
            $scope.city = {};
            $scope.street = {};
            $scope.house = {};
            $scope.houses = [];
            $scope.streets = [];
            $scope.cities = [];
            $scope.entrances = [];
            $scope.housesFilter = [];
            $scope.streetsFilter = [];
            $scope.citiesFilter = [];
            $scope.entrancesFilter = [];
        }

        function getListName(field, filter) {
            return field + (filter ? 'Filter' : '');
        }

        $scope.clear = function () {
            $scope.customer = {apartment: null, connectionDate: null, receiverType: null, receiverState: null, id: null};
            initAddressSelects();
            $scope.customerForm.$setPristine();
        };
    });
