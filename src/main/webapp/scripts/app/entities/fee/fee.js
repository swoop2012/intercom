'use strict';

angular.module('domofonApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('fee', {
                parent: 'entity',
                url: '/fee',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/fee/fees.html',
                        controller: 'FeeController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('fee');
                        return $translate.refresh();
                    }]
                }
            })
            .state('feeDetail', {
                parent: 'entity',
                url: '/fee/:id',
                data: {
                    roles: ['ADMIN']
                },
                views: {
                    'content@main': {
                        templateUrl: 'scripts/app/entities/fee/fee-detail.html',
                        controller: 'FeeDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('fee');
                        return $translate.refresh();
                    }]
                }
            });
    });
