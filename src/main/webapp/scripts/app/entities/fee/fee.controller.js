'use strict';

angular.module('domofonApp')
    .controller('FeeController', function ($scope, Fee, Customer, ngTableParams) {
        $scope.filter = {};
        $scope.customers = [];
        var PAGE_SIZE = 2;
        $scope.feeTableParams = new ngTableParams({
            page: 1,            // show first page
            count: PAGE_SIZE,   // count per page
            sorting: {date: 'desc'}
        }, {
            total: 0,   // length of data
            counts: [], // hide page counts control
            getData: function ($defer, params) {
                var keys = Object.keys(params.$params.sorting);
                var object = {
                    page: params.$params.page - 1,
                    sort: keys[0] + "," + params.$params.sorting[keys[0]]
                };
                // Filtering present on the page
                var filters = Object.keys($scope.filter);
                for (var i = 0; i < filters.length; i++) {
                    object[filters[i]] = $scope.filter[filters[i]];
                }
                object.size = PAGE_SIZE;
                // Ajax call to update the table contents
                Fee.query(object, function (data) {
                    if (data.content.length == 0 && data.number > 0) {
                        params.page(data.number);
                    }
                    else {
                        params.page(data.number + 1);
                        params.total(data.totalElements);
                        $defer.resolve(data.content);
                    }
                });
            }
        });
        $scope.refresh = function () {
            $scope.feeTableParams.reload();
        };

        $scope.refresh();

        $scope.refreshCustomers = function(search){
            if($.trim(search) != "")
                Customer.getOptions({user:search}, function(result) {
                    $scope.customers = result;
                })
        };
        $scope.create = function () {
            Fee.save($scope.fee,
                function () {
                    $scope.refresh();
                    $('#saveFeeModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.fee = Fee.get({id: id});
            $('#saveFeeModal').modal('show');
        };

        $scope.delete = function (id) {
            $scope.fee = Fee.get({id: id});
            $('#deleteFeeConfirmation').modal('show');
        };

        $scope.confirmDelete = function (id) {
            Fee.delete({id: id},
                function () {
                    $scope.refresh();
                    $('#deleteFeeConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.clear = function () {
            $scope.fee = {extCode: null, date: null, sum: null, id: null};
            $scope.feeForm.$setPristine();
        };
    });
