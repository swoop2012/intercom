'use strict';

angular.module('domofonApp')
    .controller('FeeDetailController', function ($scope, $stateParams, Fee, Customer) {
        $scope.fee = {};
        $scope.load = function (id) {
            Fee.get({id: id}, function(result) {
              $scope.fee = result;
            });
        };
        $scope.load($stateParams.id);
    });
