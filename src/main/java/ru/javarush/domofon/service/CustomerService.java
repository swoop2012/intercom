package ru.javarush.domofon.service;

import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;
import org.joda.time.DateTime;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javarush.domofon.domain.Customer;
import ru.javarush.domofon.domain.Payment;
import ru.javarush.domofon.repository.CustomerRepository;

import javax.activation.DataSource;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Tuple;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 17.02.2015.
 */
@Service
@Transactional
public class CustomerService {

    @Inject
    private CustomerRepository customerRepository;
    @Inject
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public void setCustomerBalance(Customer customer){
        if(customer == null)
            return;
        Double balance = getCurrentBalance(customer.getId());
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("id",customer.getId());
        namedParameters.put("balance",balance);
        namedParameterJdbcTemplate.update("update t_customer set balance=:balance where id=:id",namedParameters);
    }

    public Double getCustomerBalanceOffset(Long id, Integer limit){
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("id",id);
        namedParameters.put("limit", limit);
        namedParameters.put("result", Payment.Result.SUCCESS.ordinal());
        namedParameters.put("income",Payment.Operation.INCOME.ordinal());
        namedParameters.put("expense", Payment.Operation.EXPENSE.ordinal());
        return namedParameterJdbcTemplate.queryForObject("select sum(if(operation=:income,sum,0)) - sum(if(operation=:expense,sum,0)) from (select operation, sum from t_payment where customer_id=:id and result=:result order by date desc limit :limit) result", namedParameters, Double.class);
    }

    private Double getCurrentBalance(Long id){
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("id",id);
        namedParameters.put("result", Payment.Result.SUCCESS.ordinal());
        namedParameters.put("income",Payment.Operation.INCOME.ordinal());
        namedParameters.put("expense", Payment.Operation.EXPENSE.ordinal());
        return namedParameterJdbcTemplate.queryForObject("select sum(if(operation=:income,sum,0)) - sum(if(operation=:expense,sum,0)) from t_payment where customer_id=:id AND result=:result", namedParameters, Double.class);
    }

    public Map getTotalSums(Long id){
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("id",id);
        namedParameters.put("result", Payment.Result.SUCCESS.ordinal());
        namedParameters.put("income",Payment.Operation.INCOME.ordinal());
        namedParameters.put("expense", Payment.Operation.EXPENSE.ordinal());
        return namedParameterJdbcTemplate.queryForObject("select min(date) min_date, max(date) max_date, sum(if(operation=:income,sum,0)) income, sum(if(operation=:expense,sum,0)) expense from t_payment where customer_id=:id AND result=:result", namedParameters, new RowMapper<Map>() {
            @Override
            public Map mapRow(ResultSet resultSet, int i) throws SQLException {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("from",new DateTime(resultSet.getDate("min_date")));
                map.put("to",new DateTime(resultSet.getDate("max_date")));
                map.put("income",resultSet.getDouble("income"));
                map.put("expense",resultSet.getDouble("expense"));
                map.put("balance",resultSet.getDouble("income") - resultSet.getDouble("expense"));
                return map;
            }
        });
    }
}
