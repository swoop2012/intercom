package ru.javarush.domofon.service;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ru.javarush.domofon.domain.PersistentToken;
import ru.javarush.domofon.domain.User;
import ru.javarush.domofon.repository.PersistentTokenRepository;
import ru.javarush.domofon.repository.UserRepository;
import ru.javarush.domofon.security.SecurityUtils;
import ru.javarush.domofon.service.util.RandomUtil;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.*;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    @Inject
    private PasswordEncoder passwordEncoder;

    @Inject
    private UserRepository userRepository;

    @Inject
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Inject
    private PersistentTokenRepository persistentTokenRepository;


    public User createUserInformation(String login, String password, String name) {
        User newUser = new User();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(login);
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        userRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    public void updateUserInformation(String name) {
        userRepository.findOneByLogin(SecurityUtils.getCurrentLogin()).ifPresent(u -> {
            u.setFio(name);
            userRepository.save(u);
            log.debug("Changed Information for User: {}", u);
        });
    }

    public void changePassword(String password) {
        userRepository.findOneByLogin(SecurityUtils.getCurrentLogin()).ifPresent(u-> {
            String encryptedPassword = passwordEncoder.encode(password);
            u.setPassword(encryptedPassword);
            userRepository.save(u);
            log.debug("Changed password for User: {}", u);
        } );
    }

    @Transactional(readOnly = true)
    public User getUserWithAuthorities() {
        User currentUser = userRepository.findOneByLogin(SecurityUtils.getCurrentLogin()).get();
        return currentUser;
    }

    @Transactional(readOnly = true)
    public Long getCustomerIdByUserId(Long id) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        log.debug("Getting customer id for user",id);
        return namedParameterJdbcTemplate.queryForObject("select id from T_CUSTOMER where user_id=:id",map, Long.class);
    }

    @Transactional(readOnly = true)
    public Long getEngineerIdByUserId(Long id) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        log.debug("Getting engineer id for user",id);
        return namedParameterJdbcTemplate.queryForObject("select id from T_ENGINEER where user_id=:id",map, Long.class);
    }

    /**


    /**
     * Persistent Token are used for providing automatic authentication, they should be automatically deleted after
     * 30 days.
     * <p/>
     * <p>
     * This is scheduled to get fired everyday, at midnight.
     * </p>
     */
    @Scheduled(cron = "0 0 0 * * ?")
    public void removeOldPersistentTokens() {
        LocalDate now = new LocalDate();
        persistentTokenRepository.findByTokenDateBefore(now.minusMonths(1)).stream().forEach(token ->{
            log.debug("Deleting token {}", token.getSeries());
            User user = token.getUser();
            user.getPersistentTokens().remove(token);
            persistentTokenRepository.delete(token);
        });
    }


}
