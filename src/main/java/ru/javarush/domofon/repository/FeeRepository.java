package ru.javarush.domofon.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import ru.javarush.domofon.domain.Customer;
import ru.javarush.domofon.domain.Fee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;

/**
 * Spring Data JPA repository for the Fee entity.
 */
public interface FeeRepository extends JpaRepository<Fee,Long>{
    Page<Fee> findAll(Specification<Fee> specification, Pageable pageable);

    @Query(value = "SELECT f.sum FROM T_FEE f WHERE f.customer_id=?1 ORDER BY date DESC LIMIT 1", nativeQuery = true)
    BigDecimal findLastByCustomerId(Long customer);

    @Query("SELECT f.date, f.sum FROM Fee f where f.customer.id=?1")
    Page<Object[]> findAllByCustomer(Long customer, Pageable pageable);
}
