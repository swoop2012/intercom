package ru.javarush.domofon.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import ru.javarush.domofon.domain.Contract;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Contract entity.
 */
public interface ContractRepository extends JpaRepository<Contract,Long>{
	Page<Contract> findAll(Specification<Contract> specification, Pageable pageable);
}
