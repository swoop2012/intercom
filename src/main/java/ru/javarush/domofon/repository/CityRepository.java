package ru.javarush.domofon.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import ru.javarush.domofon.domain.City;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the City entity.
 */
public interface CityRepository extends JpaRepository<City,Long>{
	Page<City> findAll(Specification<City> specification, Pageable pageable);
}
