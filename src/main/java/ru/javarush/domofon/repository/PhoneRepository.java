package ru.javarush.domofon.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import ru.javarush.domofon.domain.Customer;
import ru.javarush.domofon.domain.Phone;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Spring Data JPA repository for the Phone entity.
 */
public interface PhoneRepository extends JpaRepository<Phone,Long>{
    Page<Phone> findAll(Specification<Phone> specification, Pageable pageable);

    @Query("SELECT p.number FROM Phone p where p.customer=?1")
    List<Phone> findAllByCustomer(Customer customer);
}
