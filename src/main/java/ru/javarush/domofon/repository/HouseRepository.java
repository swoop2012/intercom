package ru.javarush.domofon.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.javarush.domofon.domain.House;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the House entity.
 */
public interface HouseRepository extends JpaRepository<House,Long>{
    Page<House> findAll(Specification<House> specification, Pageable pageable);

	@Query("SELECT CASE WHEN count(house) = 0 THEN 'true' ELSE 'false' END FROM House house INNER JOIN house.street as street INNER JOIN house.street.city as city WHERE street.id = :streetId AND city.id = :cityId and house.name = :name")
	Boolean findExistsByName(@Param("name") String name, @Param("streetId") Long streetId, @Param("cityId") Long cityId);
}
