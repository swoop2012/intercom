package ru.javarush.domofon.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.javarush.domofon.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.javarush.domofon.domain.User;

import java.util.Optional;

/**
 * Spring Data JPA repository for the Customer entity.
 */
public interface CustomerRepository extends JpaRepository<Customer,Long>{
    Page<Customer> findAll(Specification<Customer> specification, Pageable pageable);

    @Query(value = "SELECT * FROM T_CUSTOMER WHERE USER_ID = :user_id", nativeQuery = true)
    Optional<Customer> findOneByUserId(@Param("user_id") Long user);
}
