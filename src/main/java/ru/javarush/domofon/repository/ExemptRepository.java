package ru.javarush.domofon.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import ru.javarush.domofon.domain.Exempt;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Exempt entity.
 */
public interface ExemptRepository extends JpaRepository<Exempt,Long>{
    Page<Exempt> findAll(Specification<Exempt> specification, Pageable pageable);
}
