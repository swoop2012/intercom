package ru.javarush.domofon.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.javarush.domofon.domain.Entrance;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Entrance entity.
 */
public interface EntranceRepository extends JpaRepository<Entrance,Long>{
	Page<Entrance> findAll(Specification<Entrance> specification, Pageable pageable);

	@Query("SELECT CASE WHEN count(entrance) = 0 THEN 'true' ELSE 'false' END FROM Entrance entrance " +
			"INNER JOIN entrance.house as house " +
			"INNER JOIN entrance.house.street as street " +
			"INNER JOIN entrance.house.street.city as city " +
			"WHERE street.id = :streetId AND city.id = :cityId AND house.id = :houseId AND entrance.name = :name")
	Boolean findExistsByName(@Param("name") String name, @Param("houseId") Long houseId, @Param("streetId") Long streetId, @Param("cityId") Long cityId);
}
