package ru.javarush.domofon.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import ru.javarush.domofon.domain.Street;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Street entity.
 */
public interface StreetRepository extends JpaRepository<Street,Long>{
	Page<Street> findAll(Specification<Street> specification, Pageable pageable);
}
