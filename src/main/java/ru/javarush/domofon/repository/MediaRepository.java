package ru.javarush.domofon.repository;

import ru.javarush.domofon.domain.Media;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Media entity.
 */
public interface MediaRepository extends JpaRepository<Media,Long>{

}
