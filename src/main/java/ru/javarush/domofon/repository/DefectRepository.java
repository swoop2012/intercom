package ru.javarush.domofon.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import ru.javarush.domofon.domain.Defect;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Spring Data JPA repository for the Defect entity.
 */
public interface DefectRepository extends JpaRepository<Defect,Long> {
    Page<Defect> findAll(Specification<Defect> specification, Pageable pageable);
}
