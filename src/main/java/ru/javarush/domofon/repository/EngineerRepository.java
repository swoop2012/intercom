package ru.javarush.domofon.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.javarush.domofon.domain.Engineer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Spring Data JPA repository for the Engineer entity.
 */
public interface EngineerRepository extends JpaRepository<Engineer,Long>{
	Page<Engineer> findAll(Specification<Engineer> specification, Pageable pageable);

	@Query(value = "SELECT * FROM T_ENGINEER WHERE USER_ID = :user_id", nativeQuery = true)
	Optional<Engineer> findOneByUserId(@Param("user_id") Long user);
}
