package ru.javarush.domofon.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import ru.javarush.domofon.domain.Customer;
import ru.javarush.domofon.domain.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.javarush.domofon.web.rest.dto.PaymentDTO;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.SqlResultSetMapping;

/**
 * Spring Data JPA repository for the Payment entity.
 */
public interface PaymentRepository extends JpaRepository<Payment,Long>{
    Page<Payment> findAll(Specification<Payment> specification, Pageable pageable);


    @Query("SELECT p.id, p.date, p.document, p.sum, p.operation, p.type, p.service, p.result, p.account FROM Payment p where p.customer.id=?1 AND p.result=0")
    Page<Object[]> findAllByCustomer(Long customer, Pageable pageable);

    @Query("SELECT p.id, p.date, p.document, p.sum, p.operation, p.type, p.service, p.result, p.account FROM Payment p where p.customer.id=?1 AND p.type=?2 AND p.operation=0")
    Page<Object[]> findAllByCustomerAndType(Long customer, Payment.Type type, Pageable pageable);
}
