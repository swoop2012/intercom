package ru.javarush.domofon.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import ru.javarush.domofon.domain.Affiliate;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Affiliate entity.
 */
public interface AffiliateRepository extends JpaRepository<Affiliate,Long>{
    Page<Affiliate> findAll(Specification<Affiliate> specification, Pageable pageable);
}
