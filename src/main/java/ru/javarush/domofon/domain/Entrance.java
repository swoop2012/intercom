package ru.javarush.domofon.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Entrance.
 */
@Entity
@Table(name = "T_ENTRANCE")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Entrance implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ext_code")
    private String extCode;

    @Column(name = "name")
    private String name;

    @Column(name = "domofon_code")
    private Integer domofonCode;

    @Column(name = "gate")
    private Boolean gate;

    @ManyToOne
    private House house;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExtCode() {
        return extCode;
    }

    public void setExtCode(String extCode) {
        this.extCode = extCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDomofonCode() {
        return domofonCode;
    }

    public void setDomofonCode(Integer domofonCode) {
        this.domofonCode = domofonCode;
    }

    public Boolean getGate() {
        return gate;
    }

    public void setGate(Boolean gate) {
        this.gate = gate;
    }

    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Entrance entrance = (Entrance) o;

        if (id != null ? !id.equals(entrance.id) : entrance.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Entrance{" +
                "id=" + id +
                ", extCode='" + extCode + "'" +
                ", name='" + name + "'" +
                ", domofonCode='" + domofonCode + "'" +
                ", gate='" + gate + "'" +
                '}';
    }
}
