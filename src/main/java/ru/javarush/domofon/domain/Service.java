package ru.javarush.domofon.domain;

/**
 * Created by User on 11.02.2015.
 */
public enum Service {
    SUBSCRIBER_FEE,
    MAINTENANCE_VIDEO_SYSTEMS,
    SERVICE_GATES,
    CALL_MASTER,
    REPAIR_DOOR_CLOSER,
    INSTALLATION_TUBE_OR_MONITOR,
    MANUFACTURING_KEYS
}
