package ru.javarush.domofon.domain.entitylistener;

import org.springframework.stereotype.Component;
import ru.javarush.domofon.domain.Payment;
import ru.javarush.domofon.service.CustomerService;

import javax.annotation.Resource;
import javax.persistence.*;
import java.lang.annotation.Annotation;

@Component
public class PaymentEntityListener {
    @Resource
    private CustomerService customerService;

    @PostPersist
    public void onPostPersist(Payment payment) {
        if(payment.getResult() == Payment.Result.SUCCESS)
            customerService.setCustomerBalance(payment.getCustomer());
    }

    @PostUpdate
    public void onPostUpdate(Payment payment) {
        customerService.setCustomerBalance(payment.getCustomer());
    }

    @PostRemove
    public void onPostRemove(Payment payment) {
        customerService.setCustomerBalance(payment.getCustomer());
    }
}
