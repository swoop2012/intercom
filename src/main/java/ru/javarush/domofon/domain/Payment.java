package ru.javarush.domofon.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.beans.factory.annotation.Configurable;
import ru.javarush.domofon.annotation.SpringEntityListeners;
import ru.javarush.domofon.domain.util.CustomDateTimeDeserializer;
import ru.javarush.domofon.domain.util.CustomDateTimeSerializer;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.joda.time.DateTime;
import ru.javarush.domofon.domain.entitylistener.PaymentEntityListener;

import javax.persistence.*;
import java.io.Serializable;

/**
 * A Payment.
 */
@Configurable
@Entity
@SpringEntityListeners(PaymentEntityListener.class)
@Table(name = "T_PAYMENT")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Payment implements Serializable {
    public enum Type{
        ONLINE,
        TICKET_WINDOW,
        BANK
    }

    public enum Operation{
        INCOME,
        EXPENSE
    }

    public enum Result{
        SUCCESS,
        FAIL
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ext_code")
    private String extCode;

    @org.hibernate.annotations.Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "date", nullable = false)
    private DateTime date;

    @Column(name = "document")
    private String document;

    @Column(name = "sum", precision=10, scale=2)
    private Double sum;

    @Column(name = "operation")
    private Operation operation;

    @Column(name = "type")
    private Type type;

    @ManyToOne
    private Customer customer;

    @Column(name = "service")
    private Service service;

    @Column(name = "result")
    private Result result;

    @Column(name = "account")
    private String account;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExtCode() {
        return extCode;
    }

    public void setExtCode(String extCode) {
        this.extCode = extCode;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Payment payment = (Payment) o;

        if (id != null ? !id.equals(payment.id) : payment.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", extCode='" + extCode + "'" +
                ", date='" + date + "'" +
                ", document='" + document + "'" +
                ", sum='" + sum + "'" +
                ", operation='" + operation + "'" +
                ", type='" + type + "'" +
                '}';
    }
}

