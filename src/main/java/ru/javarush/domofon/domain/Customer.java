package ru.javarush.domofon.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ru.javarush.domofon.domain.util.CustomDateTimeDeserializer;
import ru.javarush.domofon.domain.util.CustomDateTimeSerializer;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Customer.
 */
@Entity
@Table(name = "T_CUSTOMER")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Customer implements Serializable {
    public enum ReceiverType{
        HANDSET,
        VIDEO
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "apartment")
    private Integer apartment;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "connection_date", nullable = false)
    private DateTime connectionDate;

    @Column(name = "receiver_type")
    private ReceiverType receiverType;

    @Column(name = "receiver_state")
    private Boolean receiverState;

    @OneToOne
    private User user;

    @ManyToOne
    private Exempt exempt;

    @Column(name = "balance", precision=10, scale=2)
    private Double balance;

    @OneToOne
    private Contract contract;

    @OneToOne
    private Entrance entrance;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getApartment() {
        return apartment;
    }

    public void setApartment(Integer apartment) {
        this.apartment = apartment;
    }

    public DateTime getConnectionDate() {
        return connectionDate;
    }

    public void setConnectionDate(DateTime connectionDate) {
        this.connectionDate = connectionDate;
    }

    public ReceiverType getReceiverType() {
        return receiverType;
    }

    public void setReceiverType(ReceiverType receiverType) {
        this.receiverType = receiverType;
    }

    public Boolean getReceiverState() {
        return receiverState;
    }

    public void setReceiverState(Boolean receiverState) {
        this.receiverState = receiverState;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Exempt getExempt() {
        return exempt;
    }

    public void setExempt(Exempt exempt) {
        this.exempt = exempt;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Entrance getEntrance() {
        return entrance;
    }

    public void setEntrance(Entrance entrance) {
        this.entrance = entrance;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Customer customer = (Customer) o;

        if (id != null ? !id.equals(customer.id) : customer.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Customer{" +
            "id=" + id +
            ", apartment=" + apartment +
            ", connectionDate=" + connectionDate +
            ", receiverType=" + receiverType +
            ", receiverState=" + receiverState +
            ", user=" + user +
            ", exempt=" + exempt +
            ", balance=" + balance +
            '}';
    }
}
