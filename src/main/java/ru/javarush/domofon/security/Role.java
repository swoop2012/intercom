package ru.javarush.domofon.security;

/**
 * Constants for Spring Security authorities.
 */
public enum  Role {
    ADMIN,
    ENGINEER,
    CUSTOMER,
    EMPLOYEE
}
