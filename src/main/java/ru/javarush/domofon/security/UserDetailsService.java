package ru.javarush.domofon.security;

import ru.javarush.domofon.domain.User;
import ru.javarush.domofon.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.javarush.domofon.service.UserService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.Collections;
import java.util.List;

import static ru.javarush.domofon.security.Role.ENGINEER;

/**
 * Authenticate a user from the database.
 */
@Component("userDetailsService")
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private final Logger log = LoggerFactory.getLogger(UserDetailsService.class);

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserService userService;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String login) {
        log.debug("Authenticating {}", login);
        String lowercaseLogin = login.toLowerCase();
        Optional<User> userFromDatabase =  userRepository.findOneByLogin(lowercaseLogin);
        return userFromDatabase.map(user -> {
            List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
            Long userRoleId = null;
            if(user.getRole() != null) {
                grantedAuthorities.add(new SimpleGrantedAuthority(user.getRole().name()));
                switch (user.getRole()) {
                    case ENGINEER:
                        userRoleId = userService.getEngineerIdByUserId(user.getId());
                        break;
                    case CUSTOMER:
                        userRoleId = userService.getCustomerIdByUserId(user.getId());
                        break;
                }
            }
            return new CustomUser(lowercaseLogin,
                    user.getPassword(),
                    grantedAuthorities, user.getId(), userRoleId);
        }).orElseThrow(() -> new UsernameNotFoundException("User " + lowercaseLogin + " was not found in the database"));
    }
}
