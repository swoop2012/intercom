package ru.javarush.domofon.security;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * Created by User on 20.02.2015.
 */
public class CustomUser extends User {
    private Long id;

    private Long userRoleId;

    public CustomUser(String username, String password, Collection<? extends GrantedAuthority> authorities,Long id, Long userRoleId) {
        super(username, password, authorities);
        this.id = id;
        this.userRoleId = userRoleId;
    }

    public CustomUser(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities, Long id, Long userRoleId){
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.id = id;
        this.userRoleId = userRoleId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(Long userRoleId) {
        this.userRoleId = userRoleId;
    }
}
