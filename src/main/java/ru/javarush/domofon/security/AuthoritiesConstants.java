package ru.javarush.domofon.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    private AuthoritiesConstants() {
    }

    public static final String ADMIN = "ADMIN";

    public static final String ENGINEER = "ENGINEER";

    public static final String CUSTOMER = "CUSTOMER";

    public static final String EMPLOYEE = "EMPLOYEE";

    public static final String ANONYMOUS = "ANONYMOUS";
}
