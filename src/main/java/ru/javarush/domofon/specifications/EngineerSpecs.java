package ru.javarush.domofon.specifications;

import org.springframework.data.jpa.domain.Specification;
import ru.javarush.domofon.domain.Engineer;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public class EngineerSpecs {
	public static Specification<Engineer> getSearchCriteria(final Engineer model) {
		return (root, query, builder) -> {
			List<Predicate> predicates = new ArrayList<>();
			if(model.getId() != null && model.getId() > 0)
				predicates.add(builder.equal(root.<Integer>get("id"), model.getId()));

			if(model.getExtCode() != null && !model.getExtCode().trim().equals(""))
				predicates.add(builder.like(root.<String>get("extCode"), '%' + model.getExtCode() + '%'));

			if(model.getPhone() != null  && !model.getPhone().trim().equals(""))
				predicates.add(builder.like(root.<String>get("phone"),'%' + model.getPhone() + '%'));

			if(model.getUser() != null && model.getUser().getId() != null)
				predicates.add(builder.equal(root.<String>get("user"), model.getUser()));

			return builder.and(predicates.toArray(new Predicate[predicates.size()]));
		};
	}
}
