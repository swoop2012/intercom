package ru.javarush.domofon.specifications;


import org.springframework.data.jpa.domain.Specification;
import ru.javarush.domofon.domain.Defect;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public class DefectSpecs {
    public static Specification<Defect> getSearchCriteria(final Defect defect) {
        return (root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if(defect.getId() != null && defect.getId() > 0) {
                predicates.add(builder.equal(root.<Integer>get("id"), defect.getId()));
            }
            if(defect.getName() != null && !defect.getName().trim().equals("")) {
                predicates.add(builder.like(root.<String>get("name"), '%' +defect.getName() + '%'));
            }
            return builder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
