package ru.javarush.domofon.specifications;

import org.springframework.data.jpa.domain.Specification;
import ru.javarush.domofon.domain.*;
import ru.javarush.domofon.web.rest.dto.CustomerSearchDTO;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by User on 13.02.2015.
 */
public class CustomerSpecs {
    public static Specification<Customer> getSearchCriteria(final CustomerSearchDTO model) {
        return (root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<Predicate>();
            if(model.getId() != null && model.getId() > 0)
                predicates.add(builder.equal(root.<Integer>get("id"), model.getId()));
            if(model.getApartment() != null && model.getApartment() > 0)
                predicates.add(builder.equal(root.<Integer>get("apartment"), model.getApartment()));
            if(model.getConnectionDateFrom() != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("connectionDate"), model.getConnectionDateFrom()));
            }
            if(model.getConnectionDateTo() != null)
            {
                Date dateTo = new Date( model.getConnectionDateTo().getTime() + 86_399_999);
                predicates.add(builder.lessThanOrEqualTo(root.<Date>get("connectionDate"), dateTo));
            }
            if(model.getUser() != null && !model.getUser().trim().equals("")){
                Join<Customer, User> user = root.join(root.getModel()
                    .getSingularAttribute("user", User.class), JoinType.INNER);
                predicates.add(builder.like(user.<String>get("fio"), "%" + model.getUser() + "%"));
            }

            if(model.getContract() != null && !model.getContract().trim().equals("")){
                Join<Customer, Contract> contract = root.join("contract", JoinType.INNER);
                predicates.add(builder.equal(contract.get("number"), model.getContract()));
            }
            if(model.getReceiverType() != null)
                predicates.add(builder.equal(root.<Customer.ReceiverType>get("receiverType"),model.getReceiverType()));
            if(model.getReceiverState() != null)
                predicates.add(builder.equal(root.<Boolean>get("receiverState"),model.getReceiverState()));
            if(model.getExempt() != null)
                predicates.add(builder.equal(root.<Exempt>get("exempt"),model.getExempt()));

            if(model.getCityId() != null)
            {
                Join<Customer, Entrance> entrance = root.join("entrance",JoinType.INNER);
                Join<Entrance, House> house = entrance.join("house",JoinType.INNER);
                Join<House, Street> street = house.join("street",JoinType.INNER);
                predicates.add(builder.equal(street.<City>get("city"),model.getCityId()));
                if(model.getStreetId() != null)
                    predicates.add(builder.equal(house.<Street>get("street"),model.getStreetId()));
                if(model.getHouseId() != null)
                    predicates.add(builder.equal(entrance.<House>get("house"),model.getHouseId()));
            }
            if(model.getEntranceId() != null)
                predicates.add(builder.equal(root.<Entrance>get("entrance"),model.getEntranceId()));
            return builder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
