package ru.javarush.domofon.specifications;

import org.springframework.data.jpa.domain.Specification;
import ru.javarush.domofon.domain.Customer;
import ru.javarush.domofon.domain.Payment;
import ru.javarush.domofon.domain.Service;
import ru.javarush.domofon.domain.User;
import ru.javarush.domofon.web.rest.dto.PaymentSearchDTO;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by User on 16.02.2015.
 */
public class PaymentSpecs {
    public static Specification<Payment> getSearchCriteria(final PaymentSearchDTO model) {
        return (root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<Predicate>();
            if(model.getId() != null && model.getId() > 0)
                predicates.add(builder.equal(root.<Integer>get("id"), model.getId()));
            if(model.getDateFrom() != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("date"), model.getDateFrom()));
            }
            if(model.getDateTo() != null)
            {
                Date dateTo = new Date( model.getDateTo().getTime() + 86_399_999);
                predicates.add(builder.lessThanOrEqualTo(root.<Date>get("date"), dateTo));
            }
            if(model.getCustomer() != null && !model.getCustomer().trim().equals("")){
                Join<Payment, Customer> customer = root.join(root.getModel()
                    .getSingularAttribute("customer", Customer.class), JoinType.INNER);
                Join<Customer, User> user = customer.join("user", JoinType.INNER);
                predicates.add(builder.like(user.<String>get("fio"), "%" + model.getCustomer() + "%"));
            }
            if(model.getExtCode() != null && !model.getExtCode().trim().equals(""))
                predicates.add(builder.like(root.<String>get("extCode"), '%' + model.getExtCode() + '%'));
            if(model.getSum() != null)
                predicates.add(builder.equal(root.<String>get("sum"), model.getSum()));
            if(model.getType() != null)
                predicates.add(builder.equal(root.<Payment.Type>get("type"), model.getType()));
            if(model.getOperation() != null)
                predicates.add(builder.equal(root.<Payment.Operation>get("operation"), model.getOperation()));
            if(model.getResult() != null)
                predicates.add(builder.equal(root.<Payment.Result>get("result"), model.getResult()));
            if(model.getService() != null)
                predicates.add(builder.equal(root.<Service>get("service"), model.getService()));
            if(model.getAccount() != null)
                predicates.add(builder.like(root.<String>get("account"), '%' + model.getAccount() + '%'));
            return builder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
