package ru.javarush.domofon.specifications;

import org.springframework.data.jpa.domain.Specification;
import ru.javarush.domofon.domain.Company;
import ru.javarush.domofon.domain.Contract;
import ru.javarush.domofon.web.rest.dto.ContractSearchDTO;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ContractSpecs {
	public static Specification<Contract> getSearchCriteria(final ContractSearchDTO model) {
		return (root, query, builder) -> {
			List<Predicate> predicates = new ArrayList<>();
			if(model.getId() != null && model.getId() > 0)
				predicates.add(builder.equal(root.<Integer>get("id"), model.getId()));

			if(model.getExtCode() != null && !model.getExtCode().trim().equals(""))
				predicates.add(builder.like(root.<String>get("extCode"), '%' + model.getExtCode() + '%'));

			if(model.getNumber() != null)
				predicates.add(builder.like(root.<String>get("number").as(String.class), "%" + model.getNumber() + '%'));

			if (model.getFromDate() != null) {
				Date dateFrom = new Date( model.getFromDate().getTime());
				predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("date"), dateFrom));
			}

			if (model.getToDate() != null) {
				Date dateTo = new Date( model.getToDate().getTime() + 86_399_999);
				predicates.add(builder.lessThanOrEqualTo(root.<Date>get("date"), dateTo));
			}

			if(model.getCompany() != null && model.getCompany().getId() != null)
				predicates.add(builder.equal(root.<String>get("company"), model.getCompany()));

			return builder.and(predicates.toArray(new Predicate[predicates.size()]));
		};
	}
}
