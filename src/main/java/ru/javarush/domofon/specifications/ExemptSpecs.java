package ru.javarush.domofon.specifications;

import org.springframework.data.jpa.domain.Specification;
import ru.javarush.domofon.domain.Exempt;
import ru.javarush.domofon.web.rest.dto.ExemptSearchDTO;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by User on 16.02.2015.
 */
public class ExemptSpecs {
    public static Specification<Exempt> getSearchCriteria(final ExemptSearchDTO model) {
        return (root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<Predicate>();
            if(model.getId() != null && model.getId() > 0)
                predicates.add(builder.equal(root.<Integer>get("id"), model.getId()));
            if(model.getStartDateFrom() != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("startDate"), model.getStartDateFrom()));
            }
            if(model.getStartDateTo() != null)
            {
                Date dateTo = new Date( model.getStartDateTo().getTime() + 86_399_999);
                predicates.add(builder.lessThanOrEqualTo(root.<Date>get("startDate"), dateTo));
            }
            if(model.getEndDateFrom() != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("endDate"), model.getEndDateFrom()));
            }
            if(model.getEndDateTo() != null)
            {
                Date dateTo = new Date( model.getEndDateTo().getTime() + 86_399_999);
                predicates.add(builder.lessThanOrEqualTo(root.<Date>get("endDate"), dateTo));
            }

            if(model.getExtCode() != null && !model.getExtCode().trim().equals(""))
                predicates.add(builder.like(root.<String>get("extCode"), '%' + model.getExtCode() + '%'));
            if(model.getType() != null)
                predicates.add(builder.equal(root.<Exempt.Type>get("type"), model.getType()));
            return builder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
