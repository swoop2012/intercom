package ru.javarush.domofon.specifications;

import org.springframework.data.jpa.domain.Specification;
import ru.javarush.domofon.domain.Customer;
import ru.javarush.domofon.domain.Phone;
import ru.javarush.domofon.domain.User;
import ru.javarush.domofon.web.rest.dto.PhoneSearchDTO;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 14.02.2015.
 */
public class PhoneSpecs {
    public static Specification<Phone> getSearchCriteria(final PhoneSearchDTO model) {
        return (root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<Predicate>();
            if(model.getId() != null && model.getId() > 0)
                predicates.add(builder.equal(root.<Integer>get("id"), model.getId()));
            if(model.getCustomer() != null && !model.getCustomer().trim().equals("")){
                Join<Phone, Customer> customer = root.join(root.getModel()
                    .getSingularAttribute("customer", Customer.class), JoinType.INNER);
                Join<Customer, User> user = customer.join("user", JoinType.INNER);
                predicates.add(builder.like(user.<String>get("fio"), "%" + model.getCustomer() + "%"));
            }
            if(model.getExtCode() != null && !model.getExtCode().trim().equals(""))
                predicates.add(builder.like(root.<String>get("extCode"), '%' + model.getExtCode() + '%'));
            if(model.getNumber() != null && !model.getNumber().trim().equals(""))
                predicates.add(builder.like(root.<String>get("number"), '%' + model.getNumber() + '%'));
            return builder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
