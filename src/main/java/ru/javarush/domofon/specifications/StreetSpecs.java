package ru.javarush.domofon.specifications;

import org.springframework.data.jpa.domain.Specification;
import ru.javarush.domofon.domain.City;
import ru.javarush.domofon.domain.Street;
import ru.javarush.domofon.web.rest.dto.StreetSearchDTO;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Various on 09.02.2015.
 */
public class StreetSpecs {
	public static Specification<Street> getSearchCriteria(final StreetSearchDTO model) {
		return (root, query, builder) -> {
			List<Predicate> predicates = new ArrayList<>();
			if(model.getId() != null && model.getId() > 0)
				predicates.add(builder.equal(root.<Integer>get("id"), model.getId()));

			if(model.getExtCode() != null && !model.getExtCode().trim().equals(""))
				predicates.add(builder.like(root.<String>get("extCode"), '%' + model.getExtCode() + '%'));

			if(model.getName() != null && !model.getName().trim().equals(""))
				predicates.add(builder.like(root.<String>get("name"), '%' + model.getName() + '%'));

			if(model.getCity() != null) {
				Join<Street, City> city = root.join(root.getModel().getSingularAttribute("city", City.class), JoinType.INNER);
				predicates.add(builder.like(city.<String>get("name"), '%' + model.getCity() + '%'));
			}

			if(model.getCityId() != null) {
				predicates.add(builder.equal(root.<City>get("city"), model.getCityId()));
			}

			return builder.and(predicates.toArray(new Predicate[predicates.size()]));
		};
	}
}
