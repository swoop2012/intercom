package ru.javarush.domofon.specifications;


import org.springframework.data.jpa.domain.Specification;
import ru.javarush.domofon.domain.Affiliate;
import ru.javarush.domofon.domain.User;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;


public class AffiliateSpecs {
    public static Specification<Affiliate> getSearchCriteria(final Affiliate model) {
        return (root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if(model.getId() != null && model.getId() > 0)
                predicates.add(builder.equal(root.<Integer>get("id"), model.getId()));
            if(model.getExtCode() != null && !model.getExtCode().trim().equals(""))
                predicates.add(builder.like(root.<String>get("extCode"), '%' + model.getExtCode() + '%'));
            if(model.getName() != null && !model.getName().trim().equals(""))
                predicates.add(builder.like(root.<String>get("name"), '%' + model.getName() + '%'));
            return builder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}

