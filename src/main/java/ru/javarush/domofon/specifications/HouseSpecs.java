package ru.javarush.domofon.specifications;


import org.springframework.data.jpa.domain.Specification;
import ru.javarush.domofon.domain.City;
import ru.javarush.domofon.domain.House;
import ru.javarush.domofon.domain.Street;
import ru.javarush.domofon.web.rest.dto.HouseSearchDTO;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class HouseSpecs {
    public static Specification<House> getSearchCriteria(final HouseSearchDTO model) {
        return (Root<House> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if(model.getId() != null && model.getId() > 0)
                predicates.add(builder.equal(root.<Integer>get("id"), model.getId()));

            if(model.getExtCode() != null && !model.getExtCode().trim().equals(""))
                predicates.add(builder.like(root.<String>get("extCode"), '%' + model.getExtCode() + '%'));

            if(model.getName() != null && !model.getName().trim().equals(""))
                predicates.add(builder.like(root.<String>get("name"), '%' + model.getName() + '%'));

	        if(model.getLatitude() != null)
		        predicates.add(builder.like(root.<String>get("latitude").as(String.class), '%' + model.getLatitude().toString() + '%'));

            if(model.getLongitude() != null)
                predicates.add(builder.like(root.<String>get("longitude").as(String.class), '%' + model.getLongitude().toString() + '%'));

            if(model.getInaccurateAddress())
                predicates.add(builder.equal(root.<String>get("inaccurateAddress"), model.getInaccurateAddress()));

            if(model.getScanned())
                predicates.add(builder.equal(root.<String>get("scanned"), model.getScanned()));

	        Join<House, Street> street = root.join(root.getModel().getSingularAttribute("street", Street.class), JoinType.INNER);
	        Join<Street, City> city = street.join("city", JoinType.INNER);

	        if(model.getStreet() != null) {
		        predicates.add(builder.like(street.<String>get("name"), '%' + model.getStreet() + '%'));
	        }

	        if(model.getStreetId() != null) {
		        predicates.add(builder.equal(street.<Long>get("id"), model.getStreetId()));
	        }

	        if(model.getCityId() != null) {
		        predicates.add(builder.equal(city.<Long>get("id"), model.getCityId()));
	        }

            return builder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
