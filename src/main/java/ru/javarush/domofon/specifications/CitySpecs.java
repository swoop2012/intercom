package ru.javarush.domofon.specifications;

import org.springframework.data.jpa.domain.Specification;
import ru.javarush.domofon.domain.City;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Various on 07.02.2015.
 */
public class CitySpecs {
	public static Specification<City> getSearchCriteria(final City model) {
		return (root, query, builder) -> {
			List<Predicate> predicates = new ArrayList<>();
			if(model.getId() != null && model.getId() > 0)
				predicates.add(builder.equal(root.<Integer>get("id"), model.getId()));
			if(model.getExtCode() != null && !model.getExtCode().trim().equals(""))
				predicates.add(builder.like(root.<String>get("extCode"), '%' + model.getExtCode() + '%'));
			if(model.getName() != null && !model.getName().trim().equals(""))
				predicates.add(builder.like(root.<String>get("name"), '%' + model.getName() + '%'));
			if(model.getAffiliate() != null && model.getAffiliate().getId() != null)
				predicates.add(builder.equal(root.<String>get("affiliate"), model.getAffiliate()));
			return builder.and(predicates.toArray(new Predicate[predicates.size()]));
		};
	}
}
