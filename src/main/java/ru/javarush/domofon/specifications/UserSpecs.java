package ru.javarush.domofon.specifications;


import org.springframework.data.jpa.domain.Specification;
import ru.javarush.domofon.domain.Affiliate;
import ru.javarush.domofon.domain.User;
import ru.javarush.domofon.security.AuthoritiesConstants;
import ru.javarush.domofon.security.Role;
import ru.javarush.domofon.security.SecurityUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class UserSpecs {
    public static Specification<User> getSearchCriteria(final User model) {
        return (root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<Predicate>();
            if(model.getId() != null && model.getId() > 0)
                predicates.add(builder.equal(root.<Integer>get("id"), model.getId()));
            if(model.getFio() != null && !model.getFio().trim().equals(""))
                predicates.add(builder.like(root.<String>get("fio"), '%' + model.getFio() + '%'));
            if(model.getExtCode() != null && !model.getExtCode().trim().equals(""))
                predicates.add(builder.like(root.<String>get("extCode"), '%' + model.getExtCode() + '%'));
            if(model.getLogin() != null && !model.getLogin().trim().equals(""))
                predicates.add(builder.like(root.<String>get("login"), '%' + model.getLogin() + '%'));
            if(model.getAffiliate() != null && model.getAffiliate().getId() != null)
                predicates.add(builder.equal(root.<Affiliate>get("affiliate"),model.getAffiliate()));
            if(model.getRole() != null && model.getRole().name() != null)
                predicates.add(builder.equal(root.<Affiliate>get("role"),model.getRole()));
            if(SecurityUtils.hasAuthority(AuthoritiesConstants.EMPLOYEE)){
                List<Role> roles = new ArrayList<>();
                roles.add(Role.CUSTOMER);
                roles.add(Role.ENGINEER);
                predicates.add(root.<Affiliate>get("role").in(roles));
            }
            return builder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}

