package ru.javarush.domofon.specifications;

import org.springframework.data.jpa.domain.Specification;
import ru.javarush.domofon.domain.City;
import ru.javarush.domofon.domain.Entrance;
import ru.javarush.domofon.domain.House;
import ru.javarush.domofon.domain.Street;
import ru.javarush.domofon.web.rest.dto.EntranceSearchDTO;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Various on 21.02.2015.
 */
public class EntranceSpecs {
	public static Specification<Entrance> getSearchCriteria(final EntranceSearchDTO model) {
		return (Root<Entrance> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {
			List<Predicate> predicates = new ArrayList<>();
			if(model.getId() != null && model.getId() > 0)
				predicates.add(builder.equal(root.<Integer>get("id"), model.getId()));

			if(model.getExtCode() != null && !model.getExtCode().trim().equals(""))
				predicates.add(builder.like(root.<String>get("extCode"), '%' + model.getExtCode() + '%'));

			if(model.getName() != null && !model.getName().trim().equals(""))
				predicates.add(builder.like(root.<String>get("name"), '%' + model.getName() + '%'));

			if(model.getGate() != null)
				predicates.add(builder.equal(root.<String>get("gate"), model.getGate()));

			if(model.getDomofonCode() != null)
				predicates.add(builder.like(root.<Integer>get("domofonCode").as(String.class), "%" + model.getDomofonCode() + "%"));

			Join<Entrance, House> house = root.join("house", JoinType.INNER);
			Join<House, Street> street = house.join("street", JoinType.INNER);
			Join<Street, City> city = street.join("city", JoinType.INNER);

			if(model.getHouse() != null) {
				predicates.add(builder.like(house.<String>get("name"), '%' + model.getHouse() + '%'));
			}

            if(model.getHouseId() != null) {
                predicates.add(builder.equal(root.<House>get("house"), model.getHouseId()));
            }

			if (model.getStreetId() != null) {
				predicates.add(builder.equal(street.<Long>get("id"), model.getStreetId()));
			}

			if (model.getStreet() != null) {
				predicates.add(builder.like(street.<String>get("name"), '%' + model.getStreet() + '%'));
			}

			if (model.getCityId() != null) {
				predicates.add(builder.equal(city.<Long>get("id"), model.getCityId()));
			}

			if (model.getCity() != null) {
				predicates.add(builder.like(city.<String>get("name"), model.getCity()));
			}

			return builder.and(predicates.toArray(new Predicate[predicates.size()]));
		};
	}
}
