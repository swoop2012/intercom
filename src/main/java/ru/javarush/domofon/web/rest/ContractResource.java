package ru.javarush.domofon.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.javarush.domofon.domain.Contract;
import ru.javarush.domofon.repository.ContractRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.javarush.domofon.specifications.ContractSpecs;
import ru.javarush.domofon.web.rest.dto.ContractSearchDTO;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Contract.
 */
@RestController
@RequestMapping("/api")
public class ContractResource {

    private final Logger log = LoggerFactory.getLogger(ContractResource.class);

    @Inject
    private ContractRepository contractRepository;

    /**
     * POST  /contracts -> Create a new contract.
     */
    @RequestMapping(value = "/contracts",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Contract contract) {
        log.debug("REST request to save Contract : {}", contract);
        contractRepository.save(contract);
    }

	/**
	 * GET  /contracts -> get filtered contracts.
	 */
	@RequestMapping(value = "/contracts",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public Page<Contract> getAll(@ModelAttribute("contract") ContractSearchDTO contract, Pageable pageable) {
		log.debug("REST request to get filtered Contracts");
		return contractRepository.findAll(ContractSpecs.getSearchCriteria(contract), pageable);
	}

    /**
     * GET  /contracts -> get all the contracts.
     */
    @RequestMapping(value = "/contracts/load",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Contract> getAll() {
        log.debug("REST request to get all Contracts");
        return contractRepository.findAll();
    }

    /**
     * GET  /contracts/:id -> get the "id" contract.
     */
    @RequestMapping(value = "/contracts/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Contract> get(@PathVariable Long id) {
        log.debug("REST request to get Contract : {}", id);
        return Optional.ofNullable(contractRepository.findOne(id))
            .map(contract -> new ResponseEntity<>(
                contract,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /contracts/:id -> delete the "id" contract.
     */
    @RequestMapping(value = "/contracts/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Contract : {}", id);
        contractRepository.delete(id);
    }
}
