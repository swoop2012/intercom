package ru.javarush.domofon.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.javarush.domofon.domain.User;
import ru.javarush.domofon.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.javarush.domofon.security.AuthoritiesConstants;
import ru.javarush.domofon.security.Role;
import ru.javarush.domofon.security.SecurityUtils;
import ru.javarush.domofon.specifications.UserSpecs;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing User.
 */
@RestController
@RequestMapping("/api")
public class UserResource {

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    @Inject
    private UserRepository userRepository;

    @Inject
    private PasswordEncoder passwordEncoder;

    /**
     * POST  /users -> Create a new user.
     */
    @RequestMapping(value = "/users",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody User user) {
        log.debug("REST request to save User : {}", user);
        if(user.getId() != null && user.getId() > 0 && (user.getPassword() == null || "".equals(user.getPassword())))
        {
            User oldUser = userRepository.findOne(user.getId());
            user.setPassword(oldUser.getPassword());
            log.debug("kept old password");
        }
        else
        {
            String encoded = passwordEncoder.encode(user.getPassword());
            user.setPassword(encoded);
            log.debug("set new password");
        }
        userRepository.save(user);
    }

    /**
     * GET  /users -> get all the users.
     */
    @RequestMapping(value = "/users",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Timed
    public Page<User> getAll(@ModelAttribute("user") User user, Pageable pageable) {
        log.debug("REST request to get all Users");

        return userRepository.findAll(UserSpecs.getSearchCriteria(user), pageable);
    }

    /**
     * GET  /users -> get all the users.
     */
    @RequestMapping(value = "/users/options",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Timed
    public List<User> getOptions(@ModelAttribute("user") User user) {
        log.debug("REST request to get all Users");
        Pageable pageable = new PageRequest(0, 10,new Sort("fio"));
        user.setRole(Role.CUSTOMER);
        return userRepository.findAll(UserSpecs.getSearchCriteria(user), pageable).getContent();
    }

    /**
     * GET  /users/:id -> get the "id" user.
     */
    @RequestMapping(value = "/users/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<User> get(@PathVariable Long id) {
        log.debug("REST request to get User : {}", id);
        return Optional.ofNullable(userRepository.findOne(id))
            .map(user -> new ResponseEntity<>(
                user,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /users/:id -> delete the "id" user.
     */
    @RequestMapping(value = "/users/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete User : {}", id);
        userRepository.delete(id);
    }

    /**
     * GET  /users/:id -> get the "id" user.
     */
    @RequestMapping(value = "/users/is-field-unique",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity isFieldUnique(@RequestParam("login") String login, @RequestParam("id") Long id) {

        log.debug("validate unique login", login);
        return userRepository.findOneByLogin(login).filter(u -> {
            return u.getLogin().equals(login);
        })
        .map(u -> {
            return new ResponseEntity<String>(id.equals(u.getId()) ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
        })
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.OK));
    }

    /**
     * GET  /users/:id -> get the "id" user.
     */
    @RequestMapping(value = "/users/roles",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Role> getRoles() {
        log.debug("get roles");
        List<Role> roles = new ArrayList<Role>();
        if(SecurityUtils.hasAuthority(AuthoritiesConstants.EMPLOYEE)){
            roles.add(Role.CUSTOMER);
            roles.add(Role.ENGINEER);
        }
        else
            Collections.addAll(roles, Role.values());
        return roles;
    }



    /**
        * GET  /users/:login -> get the "login" user.
    */
    /*
    @RequestMapping(value = "/users/{login}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @RolesAllowed(AuthoritiesConstants.ADMIN)
    ResponseEntity<User> getUser(@PathVariable String login) {
        log.debug("REST request to get User : {}", login);
        return userRepository.findOneByLogin(login)
                .map(user -> new ResponseEntity<>(user, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    */
}
