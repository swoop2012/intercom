package ru.javarush.domofon.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import ru.javarush.domofon.domain.Affiliate;
import ru.javarush.domofon.repository.AffiliateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.javarush.domofon.specifications.AffiliateSpecs;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Affiliate.
 */
@RestController
@RequestMapping("/api")
@PreAuthorize("hasAnyRole('ADMIN', 'EMPLOYEE')")
public class AffiliateResource {

    private final Logger log = LoggerFactory.getLogger(AffiliateResource.class);

    @Inject
    private AffiliateRepository affiliateRepository;

    /**
     * POST  /affiliates -> Create a new affiliate.
     */
    @RequestMapping(value = "/affiliates",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @PreAuthorize("hasRole('ADMIN')")
    public void create(@RequestBody Affiliate affiliate) {
        log.debug("REST request to save Affiliate : {}", affiliate);
        affiliateRepository.save(affiliate);
    }

    /**
     * GET  /affiliates -> get filtered affiliates.
     */
    @RequestMapping(value = "/affiliates",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Page<Affiliate> getAll(@ModelAttribute("affiliate") Affiliate affiliate, Pageable pageable) {
        log.debug("REST request to get all Affiliates");
        return affiliateRepository.findAll(AffiliateSpecs.getSearchCriteria(affiliate),pageable);
    }

    /**
     * GET  /affiliates -> get all the affiliates.
     */
    @RequestMapping(value = "/affiliates/load",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Affiliate> getAll() {
        log.debug("REST request to get all Affiliates");
        return affiliateRepository.findAll();
    }

    /**
     * GET  /affiliates/:id -> get the "id" affiliate.
     */
    @RequestMapping(value = "/affiliates/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Affiliate> get(@PathVariable Long id) {
        log.debug("REST request to get Affiliate : {}", id);
        return Optional.ofNullable(affiliateRepository.findOne(id))
            .map(affiliate -> new ResponseEntity<>(
                affiliate,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /affiliates/:id -> delete the "id" affiliate.
     */
    @RequestMapping(value = "/affiliates/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @PreAuthorize("hasRole('ADMIN')")
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Affiliate : {}", id);
        affiliateRepository.delete(id);
    }
}
