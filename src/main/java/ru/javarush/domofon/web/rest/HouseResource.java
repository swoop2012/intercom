package ru.javarush.domofon.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.javarush.domofon.domain.House;
import ru.javarush.domofon.repository.HouseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.javarush.domofon.specifications.HouseSpecs;
import ru.javarush.domofon.web.rest.dto.HouseSearchDTO;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing House.
 */
@RestController
@RequestMapping("/api")
public class HouseResource {

    private final Logger log = LoggerFactory.getLogger(HouseResource.class);

    @Inject
    private HouseRepository houseRepository;

    /**
     * POST  /houses -> Create a new house.
     */
    @RequestMapping(value = "/houses",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody House house) {
        log.debug("REST request to save House : {}", house);
        houseRepository.save(house);
    }

    /**
     * GET  /cities -> get filtered houses.
     */
    @RequestMapping(value = "/houses",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Page<House> getAll(@ModelAttribute("house") HouseSearchDTO house, Pageable pageable) {
        log.debug("REST request to get filtered Cities");
        return houseRepository.findAll(HouseSpecs.getSearchCriteria(house), pageable);
    }

    /**
     * GET  /houses -> get all the houses.
     */
    @RequestMapping(value = "/houses/load",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<House> getAll() {
        log.debug("REST request to get all Houses");
        return houseRepository.findAll();
    }

    /**
     * GET  /houses/:id -> get the "id" house.
     */
    @RequestMapping(value = "/houses/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<House> get(@PathVariable Long id) {
        log.debug("REST request to get House : {}", id);
        return Optional.ofNullable(houseRepository.findOne(id))
            .map(house -> new ResponseEntity<>(
                house,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /houses/:id -> delete the "id" house.
     */
    @RequestMapping(value = "/houses/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete House : {}", id);
        houseRepository.delete(id);
    }

	/**
	 * GET  /houses/is-field-unique -> check for name uniqueness.
	 */
	@RequestMapping(value = "/houses/is-name-unique",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity isNameUnique(@ModelAttribute("house") HouseSearchDTO house) {
		log.debug("validate unique house name", house);

		return houseRepository.findExistsByName(house.getName(), house.getStreetId(), house.getCityId())
				? new ResponseEntity(HttpStatus.OK)
				: new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
