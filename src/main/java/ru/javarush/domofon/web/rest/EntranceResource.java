package ru.javarush.domofon.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.javarush.domofon.domain.Entrance;
import ru.javarush.domofon.repository.EntranceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.javarush.domofon.specifications.EntranceSpecs;
import ru.javarush.domofon.web.rest.dto.EntranceSearchDTO;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Entrance.
 */
@RestController
@RequestMapping("/api")
public class EntranceResource {

    private final Logger log = LoggerFactory.getLogger(EntranceResource.class);

    @Inject
    private EntranceRepository entranceRepository;

    /**
     * POST  /entrances -> Create a new entrance.
     */
    @RequestMapping(value = "/entrances",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Entrance entrance) {
        log.debug("REST request to save Entrance : {}", entrance);
        entranceRepository.save(entrance);
    }

	/**
	 * GET  /entrances -> get filtered entrances.
	 */
	@RequestMapping(value = "/entrances",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public Page<Entrance> getAll(@ModelAttribute("entrance") EntranceSearchDTO entrance, Pageable pageable) {
		log.debug("REST request to get filtered Entrances");
		return entranceRepository.findAll(EntranceSpecs.getSearchCriteria(entrance), pageable);
	}

    /**
     * GET  /entrances/load -> get all the entrances.
     */
    @RequestMapping(value = "/entrances/load",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Entrance> getAll() {
        log.debug("REST request to get all Entrances");
        return entranceRepository.findAll();
    }

    /**
     * GET  /entrances/:id -> get the "id" entrance.
     */
    @RequestMapping(value = "/entrances/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Entrance> get(@PathVariable Long id) {
        log.debug("REST request to get Entrance : {}", id);
        return Optional.ofNullable(entranceRepository.findOne(id))
            .map(entrance -> new ResponseEntity<>(
                entrance,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /entrances/:id -> delete the "id" entrance.
     */
    @RequestMapping(value = "/entrances/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Entrance : {}", id);
        entranceRepository.delete(id);
    }

	/**
	 * GET  /entrances/is-name-unique -> check for name uniqueness.
	 */
	@RequestMapping(value = "/entrances/is-name-unique",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity isNameUnique(@ModelAttribute("entrance") EntranceSearchDTO entrance) {
		log.debug("validate unique house name", entrance);

		return entranceRepository.findExistsByName(entrance.getName(), entrance.getHouseId(), entrance.getStreetId(), entrance.getCityId())
				? new ResponseEntity(HttpStatus.OK)
				: new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
