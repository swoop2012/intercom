package ru.javarush.domofon.web.rest;

import com.codahale.metrics.annotation.Timed;
import ru.javarush.domofon.domain.Media;
import ru.javarush.domofon.repository.MediaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Media.
 */
@RestController
@RequestMapping("/api")
public class MediaResource {

    private final Logger log = LoggerFactory.getLogger(MediaResource.class);

    @Inject
    private MediaRepository mediaRepository;

    /**
     * POST  /medias -> Create a new media.
     */
    @RequestMapping(value = "/medias",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Media media) {
        log.debug("REST request to save Media : {}", media);
        mediaRepository.save(media);
    }

    /**
     * GET  /medias -> get all the medias.
     */
    @RequestMapping(value = "/medias",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Media> getAll() {
        log.debug("REST request to get all Medias");
        return mediaRepository.findAll();
    }

    /**
     * GET  /medias/:id -> get the "id" media.
     */
    @RequestMapping(value = "/medias/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Media> get(@PathVariable Long id) {
        log.debug("REST request to get Media : {}", id);
        return Optional.ofNullable(mediaRepository.findOne(id))
            .map(media -> new ResponseEntity<>(
                media,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /medias/:id -> delete the "id" media.
     */
    @RequestMapping(value = "/medias/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Media : {}", id);
        mediaRepository.delete(id);
    }
}
