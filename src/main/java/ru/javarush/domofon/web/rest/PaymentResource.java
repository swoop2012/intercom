package ru.javarush.domofon.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.javarush.domofon.domain.Payment;
import ru.javarush.domofon.domain.Service;
import ru.javarush.domofon.repository.PaymentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.javarush.domofon.specifications.PaymentSpecs;
import ru.javarush.domofon.web.rest.dto.PaymentSearchDTO;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Payment.
 */
@RestController
@RequestMapping("/api")
public class PaymentResource {

    private final Logger log = LoggerFactory.getLogger(PaymentResource.class);

    @Inject
    private PaymentRepository paymentRepository;

    /**
     * POST  /payments -> Create a new payment.
     */
    @RequestMapping(value = "/payments",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Payment payment) {
        log.debug("REST request to save Payment : {}", payment);
        paymentRepository.save(payment);
    }

    /**
     * GET  /payments -> get all the payments.
     */
    @RequestMapping(value = "/payments",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Page<Payment> getAll(@ModelAttribute("payment") PaymentSearchDTO payment, Pageable pageable) {
        log.debug("REST request to get all Payments");
        return paymentRepository.findAll(PaymentSpecs.getSearchCriteria(payment), pageable);
    }

    /**
     * GET  /exempts -> get all the payment types.
     */
    @RequestMapping(value = "/payments/types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Payment.Type> getTypes() {
        log.debug("REST request to get all payment types");
        List<Payment.Type> types = new ArrayList<>();
        Collections.addAll(types, Payment.Type.values());
        return types;
    }

    /**
     * GET  /exempts -> get all the services.
     */
    @RequestMapping(value = "/payments/services",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Service> getServices() {
        log.debug("REST request to get all Services");
        List<Service> services = new ArrayList<>();
        Collections.addAll(services, Service.values());
        return services;
    }


    /**
     * GET  /exempts -> get all the results.
     */
    @RequestMapping(value = "/payments/results",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Payment.Result> getResults() {
        log.debug("REST request to get all Result types");
        List<Payment.Result> results = new ArrayList<>();
        Collections.addAll(results, Payment.Result.values());
        return results;
    }


    /**
     * GET  /exempts -> get all the payment types.
     */
    @RequestMapping(value = "/payments/operations",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Payment.Operation> getOperations() {
        log.debug("REST request to get all Operation types");
        List<Payment.Operation> operations = new ArrayList<>();
        Collections.addAll(operations, Payment.Operation.values());
        return operations;
    }


    /**
     * GET  /payments/:id -> get the "id" payment.
     */
    @RequestMapping(value = "/payments/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Payment> get(@PathVariable Long id) {
        log.debug("REST request to get Payment : {}", id);
        return Optional.ofNullable(paymentRepository.findOne(id))
            .map(payment -> new ResponseEntity<>(
                payment,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /payments/:id -> delete the "id" payment.
     */
    @RequestMapping(value = "/payments/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Payment : {}", id);
        paymentRepository.delete(id);
    }
}
