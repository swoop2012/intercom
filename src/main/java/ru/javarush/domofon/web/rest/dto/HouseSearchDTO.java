package ru.javarush.domofon.web.rest.dto;

import java.math.BigDecimal;

/**
 * Created by Various on 22.02.2015.
 */
public class HouseSearchDTO {
	private Long id;

	private String extCode;

	private String name;

	private String street;

	private Long streetId;

	private Long cityId;

	private BigDecimal latitude;

	private BigDecimal longitude;

	private Boolean inaccurateAddress;

	private Boolean scanned;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getExtCode() {
		return extCode;
	}

	public void setExtCode(String extCode) {
		this.extCode = extCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public Boolean getInaccurateAddress() {
		return inaccurateAddress;
	}

	public void setInaccurateAddress(Boolean inaccurateAddress) {
		this.inaccurateAddress = inaccurateAddress;
	}

	public Boolean getScanned() {
		return scanned;
	}

	public void setScanned(Boolean scanned) {
		this.scanned = scanned;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public Long getStreetId() {
		return streetId;
	}

	public void setStreetId(Long streetId) {
		this.streetId = streetId;
	}
}
