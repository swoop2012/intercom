package ru.javarush.domofon.web.rest.dto;

import org.springframework.format.annotation.DateTimeFormat;
import ru.javarush.domofon.domain.Payment;
import ru.javarush.domofon.domain.Service;

import java.util.Date;

/**
 * Created by User on 16.02.2015.
 */
public class PaymentSearchDTO {
    private Long id;

    private String extCode;

    private String customer;

    private String sum;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date dateFrom;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date dateTo;

    private Payment.Operation operation;

    private Payment.Type type;

    private Payment.Result result;

    private Service service;

    private String account;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExtCode() {
        return extCode;
    }

    public void setExtCode(String extCode) {
        this.extCode = extCode;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Payment.Operation getOperation() {
        return operation;
    }

    public void setOperation(Payment.Operation operation) {
        this.operation = operation;
    }

    public Payment.Type getType() {
        return type;
    }

    public void setType(Payment.Type type) {
        this.type = type;
    }

    public Payment.Result getResult() {
        return result;
    }

    public void setResult(Payment.Result result) {
        this.result = result;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
