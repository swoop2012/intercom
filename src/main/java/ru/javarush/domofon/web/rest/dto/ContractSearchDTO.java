package ru.javarush.domofon.web.rest.dto;

import org.springframework.format.annotation.DateTimeFormat;
import ru.javarush.domofon.domain.Company;

import java.util.Date;

/**
 * Created by Various on 20.02.2015.
 */
public class ContractSearchDTO {
	private Long id;

	private String extCode;

	private Integer number;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private Date fromDate;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private Date toDate;

	private Company company;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getExtCode() {
		return extCode;
	}

	public void setExtCode(String extCode) {
		this.extCode = extCode;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
}
