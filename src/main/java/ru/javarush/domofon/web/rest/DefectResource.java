package ru.javarush.domofon.web.rest;

import com.codahale.metrics.annotation.Timed;
import ru.javarush.domofon.domain.Defect;
import ru.javarush.domofon.repository.DefectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Defect.
 */
@RestController
@RequestMapping("/api")
public class DefectResource {

    private final Logger log = LoggerFactory.getLogger(DefectResource.class);

    @Inject
    private DefectRepository defectRepository;

    /**
     * POST  /defects -> Create a new defect.
     */
    @RequestMapping(value = "/defects",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Defect defect) {
        log.debug("REST request to save Defect : {}", defect);
        defectRepository.save(defect);
    }

    /**
     * GET  /defects -> get all the defects.
     */
    @RequestMapping(value = "/defects",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Defect> getAll() {
        log.debug("REST request to get all Defects");
        return defectRepository.findAll();
    }

    /**
     * GET  /defects/:id -> get the "id" defect.
     */
    @RequestMapping(value = "/defects/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Defect> get(@PathVariable Long id) {
        log.debug("REST request to get Defect : {}", id);
        return Optional.ofNullable(defectRepository.findOne(id))
            .map(defect -> new ResponseEntity<>(
                defect,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /defects/:id -> delete the "id" defect.
     */
    @RequestMapping(value = "/defects/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Defect : {}", id);
        defectRepository.delete(id);
    }
}
