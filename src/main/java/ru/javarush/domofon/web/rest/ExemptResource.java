package ru.javarush.domofon.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.javarush.domofon.domain.Exempt;
import ru.javarush.domofon.repository.ExemptRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.javarush.domofon.specifications.ExemptSpecs;
import ru.javarush.domofon.web.rest.dto.ExemptSearchDTO;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Exempt.
 */
@RestController
@RequestMapping("/api")
public class ExemptResource {

    private final Logger log = LoggerFactory.getLogger(ExemptResource.class);

    @Inject
    private ExemptRepository exemptRepository;

    /**
     * POST  /exempts -> Create a new exempt.
     */
    @RequestMapping(value = "/exempts",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Exempt exempt) {
        log.debug("REST request to save Exempt : {}", exempt);
        exemptRepository.save(exempt);
    }

    /**
     * GET  /exempts -> get all the exempts(filtered and paginated).
     */
    @RequestMapping(value = "/exempts",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Page<Exempt> getAll(@ModelAttribute("exempt") ExemptSearchDTO exempt, Pageable pageable) {
        log.debug("REST request to get all Exempts");
        return exemptRepository.findAll(ExemptSpecs.getSearchCriteria(exempt), pageable);
    }

    /**
     * GET  /exempts -> get all the exempts.
     */
    @RequestMapping(value = "/exempts/load-all",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Exempt> loadAll() {
        log.debug("REST request to get all Exempts");
        return exemptRepository.findAll();
    }

    /**
     * GET  /exempts -> get all the exempt types.
     */
    @RequestMapping(value = "/exempts/types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Exempt.Type> getTypes() {
        log.debug("REST request to get all Exempt types");
        List<Exempt.Type> types = new ArrayList<>();
        Collections.addAll(types, Exempt.Type.values());
        return types;
    }


    /**
     * GET  /exempts/:id -> get the "id" exempt.
     */
    @RequestMapping(value = "/exempts/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Exempt> get(@PathVariable Long id) {
        log.debug("REST request to get Exempt : {}", id);
        return Optional.ofNullable(exemptRepository.findOne(id))
            .map(exempt -> new ResponseEntity<>(
                exempt,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /exempts/:id -> delete the "id" exempt.
     */
    @RequestMapping(value = "/exempts/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Exempt : {}", id);
        exemptRepository.delete(id);
    }
}
