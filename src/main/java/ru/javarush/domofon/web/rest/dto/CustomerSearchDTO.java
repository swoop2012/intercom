package ru.javarush.domofon.web.rest.dto;

import org.springframework.format.annotation.DateTimeFormat;
import ru.javarush.domofon.domain.Customer;
import ru.javarush.domofon.domain.Exempt;
import ru.javarush.domofon.domain.User;

import java.util.Date;

public class CustomerSearchDTO {
    private Long id;

    private Integer apartment;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date connectionDateFrom;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date connectionDateTo;

    private Customer.ReceiverType receiverType;

    private Boolean receiverState;

    private String user;

    private Exempt exempt;

    private String contract;

    private Long cityId;

    private Long streetId;

    private Long houseId;

    private Long entranceId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getApartment() {
        return apartment;
    }

    public void setApartment(Integer apartment) {
        this.apartment = apartment;
    }

    public Date getConnectionDateFrom() {
        return connectionDateFrom;
    }

    public void setConnectionDateFrom(Date connectionDateFrom) {
        this.connectionDateFrom = connectionDateFrom;
    }

    public Date getConnectionDateTo() {
        return connectionDateTo;
    }

    public void setConnectionDateTo(Date connectionDateTo) {
        this.connectionDateTo = connectionDateTo;
    }

    public Customer.ReceiverType getReceiverType() {
        return receiverType;
    }

    public void setReceiverType(Customer.ReceiverType receiverType) {
        this.receiverType = receiverType;
    }

    public Boolean getReceiverState() {
        return receiverState;
    }

    public void setReceiverState(Boolean receiverState) {
        this.receiverState = receiverState;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Exempt getExempt() {
        return exempt;
    }

    public void setExempt(Exempt exempt) {
        this.exempt = exempt;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getStreetId() {
        return streetId;
    }

    public void setStreetId(Long streetId) {
        this.streetId = streetId;
    }

    public Long getHouseId() {
        return houseId;
    }

    public void setHouseId(Long houseId) {
        this.houseId = houseId;
    }

    public Long getEntranceId() {
        return entranceId;
    }

    public void setEntranceId(Long entranceId) {
        this.entranceId = entranceId;
    }
}
