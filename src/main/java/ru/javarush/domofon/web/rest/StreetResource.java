package ru.javarush.domofon.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.javarush.domofon.domain.Street;
import ru.javarush.domofon.repository.StreetRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.javarush.domofon.specifications.StreetSpecs;
import ru.javarush.domofon.web.rest.dto.StreetSearchDTO;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Street.
 */
@RestController
@RequestMapping("/api")
public class StreetResource {

    private final Logger log = LoggerFactory.getLogger(StreetResource.class);

    @Inject
    private StreetRepository streetRepository;

    /**
     * POST  /streets -> Create a new street.
     */
    @RequestMapping(value = "/streets",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Street street) {
        log.debug("REST request to save Street : {}", street);
        streetRepository.save(street);
    }

	/**
	 * GET /streets -> get filtered streets
	 */
	@RequestMapping(value = "/streets",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public Page<Street> getAll(@ModelAttribute("street") StreetSearchDTO street, Pageable pageable) {
		log.debug("REST request to get filtered Streets");
		return streetRepository.findAll(StreetSpecs.getSearchCriteria(street), pageable);
	}

    /**
     * GET  /streets -> get all the affiliates.
     */
    @RequestMapping(value = "/streets/load",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Street> getAll() {
        log.debug("REST request to get all Streets");
        return streetRepository.findAll();
    }

    /**
     * GET  /streets/:id -> get the "id" street.
     */
    @RequestMapping(value = "/streets/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Street> get(@PathVariable Long id) {
        log.debug("REST request to get Street : {}", id);
        return Optional.ofNullable(streetRepository.findOne(id))
            .map(street -> new ResponseEntity<>(
                street,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /streets/:id -> delete the "id" street.
     */
    @RequestMapping(value = "/streets/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Street : {}", id);
        streetRepository.delete(id);
    }
}
