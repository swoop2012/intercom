package ru.javarush.domofon.web.rest.dto;

/**
 * Created by Various on 21.02.2015.
 */
public class EntranceSearchDTO {
	private Long id;

	private String extCode;

	private String name;

	private Integer domofonCode;

	private Boolean gate;

	private String house;

	private String street;

	private String city;

	private Long houseId;

	private Long streetId;

	private Long cityId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getExtCode() {
		return extCode;
	}

	public void setExtCode(String extCode) {
		this.extCode = extCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getDomofonCode() {
		return domofonCode;
	}

	public void setDomofonCode(Integer domofonCode) {
		this.domofonCode = domofonCode;
	}

	public Boolean getGate() {
		return gate;
	}

	public void setGate(Boolean gate) {
		this.gate = gate;
	}

	public String getHouse() {
		return house;
	}

	public void setHouse(String house) {
		this.house = house;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Long getHouseId() {
		return houseId;
	}

	public void setHouseId(Long houseId) {
		this.houseId = houseId;
	}

	public Long getStreetId() {
		return streetId;
	}

	public void setStreetId(Long streetId) {
		this.streetId = streetId;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}
}
