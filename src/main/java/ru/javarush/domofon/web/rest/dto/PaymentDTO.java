package ru.javarush.domofon.web.rest.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.DateTime;
import ru.javarush.domofon.domain.Payment;
import ru.javarush.domofon.domain.Service;
import ru.javarush.domofon.domain.util.CustomDateTimeDeserializer;
import ru.javarush.domofon.domain.util.CustomDateTimeSerializer;

/**
 * Created by User on 22.02.2015.
 */
public class PaymentDTO {
    private Long id;
    private DateTime date;
    private String document;
    private Double sum;
    private Payment.Operation operation;
    private Payment.Type type;
    private Service service;
    private Payment.Result result;
    private String account;

    private static final int ID_POS = 0;
    private static final int DATE_POS = 1;
    private static final int DOCUMENT_POS = 2;
    private static final int SUM_POS = 3;
    private static final int OPERATION_POS = 4;
    private static final int TYPE_POS = 5;
    private static final int SERVICE_POS = 6;
    private static final int RESULT_POS = 7;
    private static final int ACCOUNT_POS = 8;


    public PaymentDTO(Object[] objects) {
        if(objects != null && objects.length == 9)
        {
            this.id = (Long) objects[ID_POS];
            this.date = (DateTime) objects[DATE_POS];
            this.document = (String) objects[DOCUMENT_POS];
            this.sum = (Double) objects[SUM_POS];
            this.operation = (Payment.Operation) objects[OPERATION_POS];
            this.type = (Payment.Type) objects[TYPE_POS];
            this.service = (Service) objects[SERVICE_POS];
            this.result = (Payment.Result) objects[RESULT_POS];
            this.account = (String) objects[ACCOUNT_POS];
        }
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public Payment.Operation getOperation() {
        return operation;
    }

    public void setOperation(Payment.Operation operation) {
        this.operation = operation;
    }

    public Payment.Type getType() {
        return type;
    }

    public void setType(Payment.Type type) {
        this.type = type;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Payment.Result getResult() {
        return result;
    }

    public void setResult(Payment.Result result) {
        this.result = result;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
