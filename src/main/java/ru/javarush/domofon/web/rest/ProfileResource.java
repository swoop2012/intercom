package ru.javarush.domofon.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.javarush.domofon.domain.Payment;
import ru.javarush.domofon.domain.Phone;
import ru.javarush.domofon.repository.CustomerRepository;
import ru.javarush.domofon.repository.FeeRepository;
import ru.javarush.domofon.repository.PaymentRepository;
import ru.javarush.domofon.repository.PhoneRepository;
import ru.javarush.domofon.security.CustomUser;
import ru.javarush.domofon.service.CustomerService;
import ru.javarush.domofon.web.rest.dto.FeeDTO;
import ru.javarush.domofon.web.rest.dto.PaymentDTO;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

/**
 * REST controller for customers profile.
 */
@RestController
@RequestMapping("/api/profile")
@PreAuthorize("hasRole('CUSTOMER')")
public class ProfileResource {

    private final Logger log = LoggerFactory.getLogger(ProfileResource.class);

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private PaymentRepository paymentRepository;

    @Inject
    private FeeRepository feeRepository;

    @Inject
    private PhoneRepository phoneRepository;

    @Inject
    private CustomerService customerService;

    /**
     * GET  /balance -> get customer data.
     */
    @RequestMapping(value = "/balance",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Map<String, Object> getBalance(@AuthenticationPrincipal CustomUser user) {
        log.debug("REST request to get customer balance");
        return customerRepository.findOneByUserId(user.getId())
            .map(u -> {
                Map<String, Object> map = new HashMap<String, Object>();
                Set<Phone> phones = new HashSet<Phone>();
                phones.addAll(phoneRepository.findAllByCustomer(u));
                map.put("customer", u);
                map.put("phones", phones);
                map.put("lastFee", feeRepository.findLastByCustomerId(u.getId()));
                return map;
            })
            .orElseGet(() -> null);
    }

    /**
     * GET  /cash-flow -> get customer cash flow.
     */
    @RequestMapping(value = "/cash-flow",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Map<String, Object> getCashFlow(@AuthenticationPrincipal CustomUser user, Pageable pageable) {
        log.debug("REST request to get all payments");
        Map<String, Object> map = new HashMap<>();
        Page<Object[]> page = paymentRepository.findAllByCustomer(user.getUserRoleId(), pageable);
        List<PaymentDTO> payments = page.getContent().stream().map(PaymentDTO::new).collect(Collectors.toList());
        Double balanceOffset = customerService.getCustomerBalanceOffset(user.getUserRoleId(), pageable.getOffset());
        map.put("page", new PageImpl<PaymentDTO>(payments,pageable, page.getTotalElements()));
        map.put("balanceOffset", balanceOffset != null ? balanceOffset : 0d);
        return map;

    }

    /**
     * GET  /cash-flow -> get customer cash flow total.
     */
    @RequestMapping(value = "/cash-flow-total",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Map getCashFlowTotal(@AuthenticationPrincipal CustomUser user) {
        log.debug("REST request to get total sums");
        return customerService.getTotalSums(user.getUserRoleId());
    }


    /**
     * GET  /fee-history -> get customer fee history.
     */
    @RequestMapping(value = "/fee-history",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Page<FeeDTO> getFeeHistory(@AuthenticationPrincipal CustomUser user,Pageable pageable) {
        log.debug("REST request to get all fees");
        Page<Object[]> page = feeRepository.findAllByCustomer(user.getUserRoleId(), pageable);
        List<FeeDTO> payments = page.getContent().stream().map(FeeDTO::new).collect(Collectors.toList());
        return new PageImpl<FeeDTO>(payments,pageable, page.getTotalElements());
    }


    /**
     * GET  /payment-history -> get customer payment history.
     */
    @RequestMapping(value = "/payment-history",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Map<String, Object> getPaymentHistory(@AuthenticationPrincipal CustomUser user, Pageable pageable) {
        log.debug("REST request to get all online payments");
        Map<String, Object> map = new HashMap<>();
        Page<Object[]> page = paymentRepository.findAllByCustomerAndType(user.getUserRoleId(), Payment.Type.ONLINE, pageable);
        List<PaymentDTO> payments = page.getContent().stream().map(PaymentDTO::new).collect(Collectors.toList());
        map.put("page", new PageImpl<PaymentDTO>(payments,pageable, page.getTotalElements()));
        map.put("customer", customerRepository.findOne(user.getUserRoleId()));
        return map;
    }




}
