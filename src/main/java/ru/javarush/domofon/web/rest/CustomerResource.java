package ru.javarush.domofon.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import ru.javarush.domofon.domain.Customer;
import ru.javarush.domofon.domain.User;
import ru.javarush.domofon.repository.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.javarush.domofon.specifications.CustomerSpecs;
import ru.javarush.domofon.web.rest.dto.CustomerSearchDTO;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Customer.
 */
@RestController
@RequestMapping("/api")
public class CustomerResource {

    private final Logger log = LoggerFactory.getLogger(CustomerResource.class);

    @Inject
    private CustomerRepository customerRepository;

    /**
     * POST  /customers -> Create a new customer.
     */
    @RequestMapping(value = "/customers",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Customer customer) {
        log.debug("REST request to save Customer : {}", customer);
        customerRepository.save(customer);
    }

    /**
     * GET  /customers -> get all the customers.
     */
    @RequestMapping(value = "/customers",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Page<Customer> getAll(@ModelAttribute("customer") CustomerSearchDTO customer, Pageable pageable) {
        log.debug("REST request to get all Customers");
        return customerRepository.findAll(CustomerSpecs.getSearchCriteria(customer), pageable);
    }

    @RequestMapping(value = "/customers/receiver-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Customer.ReceiverType> getReceiverTypes() {
        log.debug("REST request to get all receiverTypes");
        List<Customer.ReceiverType> types = new ArrayList<Customer.ReceiverType>();
        Collections.addAll(types, Customer.ReceiverType.values());
        return types;
    }

    /**
     * GET  /customers/:id -> get the "id" customer.
     */
    @RequestMapping(value = "/customers/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Customer> get(@PathVariable Long id) {
        log.debug("REST request to get Customer : {}", id);
        return Optional.ofNullable(customerRepository.findOne(id))
            .map(customer -> new ResponseEntity<>(
                customer,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /customers/:id -> delete the "id" customer.
     */
    @RequestMapping(value = "/customers/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Customer : {}", id);
        customerRepository.delete(id);
    }

    /**
     * GET  /users/:id -> get the "id" user.
     */
    @RequestMapping(value = "/customers/is-field-unique",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity isFieldUnique(@RequestParam("user") Long user, @RequestParam("id") Long id) {

        log.debug("validate unique login", user);
        return customerRepository.findOneByUserId(user).filter(u -> {
            return u.getUser().getId().equals(user);
        })
        .map(u -> {
            return new ResponseEntity<String>(id.equals(u.getId()) ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
        })
            .orElseGet(() -> new ResponseEntity<>(HttpStatus.OK));
    }

    /**
     * GET  /users -> get all the users.
     */
    @RequestMapping(value = "/customers/options",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Timed
    public List<Customer> getOptions(@ModelAttribute("customer") CustomerSearchDTO customer) {
        log.debug("REST request to get all Users");
        Pageable pageable = new PageRequest(0, 10);
        return customerRepository.findAll(CustomerSpecs.getSearchCriteria(customer), pageable).getContent();
    }
}
