package ru.javarush.domofon.web.rest.dto;

import org.joda.time.DateTime;
import ru.javarush.domofon.domain.Payment;
import ru.javarush.domofon.domain.Service;

import java.math.BigDecimal;

/**
 * Created by User on 22.02.2015.
 */
public class FeeDTO {
    private DateTime date;
    private BigDecimal sum;

    private static final int DATE_POS = 0;
    private static final int SUM_POS = 1;

    public FeeDTO(Object[] objects) {
        if(objects != null && objects.length == 2)
        {
            this.date = (DateTime) objects[DATE_POS];
            this.sum = (BigDecimal) objects[SUM_POS];
        }
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }
}
