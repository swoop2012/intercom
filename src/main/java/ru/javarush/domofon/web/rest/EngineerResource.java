package ru.javarush.domofon.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import ru.javarush.domofon.domain.Engineer;
import ru.javarush.domofon.repository.EngineerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.javarush.domofon.specifications.EngineerSpecs;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Engineer.
 */
@RestController
@RequestMapping("/api")
public class EngineerResource {

    private final Logger log = LoggerFactory.getLogger(EngineerResource.class);

    @Inject
    private EngineerRepository engineerRepository;

    /**
     * POST  /engineers -> Create a new engineer.
     */
    @RequestMapping(value = "/engineers",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Engineer engineer) {
        log.debug("REST request to save Engineer : {}", engineer);
        engineerRepository.save(engineer);
    }

	/**
	 * GET  /engineers -> get filtered engineers.
	 */
	@RequestMapping(value = "/engineers",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public Page<Engineer> getAll(@ModelAttribute("engineer") Engineer engineer, Pageable pageable) {
		log.debug("REST request to get filtered Engineers");
		return engineerRepository.findAll(EngineerSpecs.getSearchCriteria(engineer), pageable);
	}

    /**
     * GET  /engineers/load -> get all the engineers.
     */
    @RequestMapping(value = "/engineers/load",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Engineer> getAll() {
        log.debug("REST request to get all Engineers");
        return engineerRepository.findAll();
    }

    /**
     * GET  /engineers/:id -> get the "id" engineer.
     */
    @RequestMapping(value = "/engineers/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Engineer> get(@PathVariable Long id) {
        log.debug("REST request to get Engineer : {}", id);
        return Optional.ofNullable(engineerRepository.findOne(id))
            .map(engineer -> new ResponseEntity<>(
                engineer,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /engineers/:id -> delete the "id" engineer.
     */
    @RequestMapping(value = "/engineers/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Engineer : {}", id);
        engineerRepository.delete(id);
    }

	/**
	 * GET  /users/:id -> get the "id" user.
	 */
	@RequestMapping(value = "/engineers/is-field-unique",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity isFieldUnique(@RequestParam("user") Long user, @RequestParam("id") Long id) {

		log.debug("validate unique login", user);
		return engineerRepository.findOneByUserId(user).filter(u -> u.getUser().getId().equals(user))
				.map(u -> new ResponseEntity<String>(id.equals(u.getId()) ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR))
				.orElseGet(() -> new ResponseEntity<>(HttpStatus.OK));
	}
}
