package ru.javarush.domofon.web.rest.dto;

import org.springframework.format.annotation.DateTimeFormat;
import ru.javarush.domofon.domain.Exempt;

import java.util.Date;

/**
 * Created by User on 16.02.2015.
 */
public class ExemptSearchDTO {
    private Long id;

    private String extCode;

    private Exempt.Type type;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date startDateFrom;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date startDateTo;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date endDateFrom;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date endDateTo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExtCode() {
        return extCode;
    }

    public void setExtCode(String extCode) {
        this.extCode = extCode;
    }

    public Exempt.Type getType() {
        return type;
    }

    public void setType(Exempt.Type type) {
        this.type = type;
    }

    public Date getStartDateFrom() {
        return startDateFrom;
    }

    public void setStartDateFrom(Date startDateFrom) {
        this.startDateFrom = startDateFrom;
    }

    public Date getStartDateTo() {
        return startDateTo;
    }

    public void setStartDateTo(Date startDateTo) {
        this.startDateTo = startDateTo;
    }

    public Date getEndDateFrom() {
        return endDateFrom;
    }

    public void setEndDateFrom(Date endDateFrom) {
        this.endDateFrom = endDateFrom;
    }

    public Date getEndDateTo() {
        return endDateTo;
    }

    public void setEndDateTo(Date endDateTo) {
        this.endDateTo = endDateTo;
    }
}
