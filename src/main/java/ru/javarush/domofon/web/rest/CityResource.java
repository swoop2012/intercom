package ru.javarush.domofon.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.javarush.domofon.domain.City;
import ru.javarush.domofon.repository.CityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.javarush.domofon.specifications.CitySpecs;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing City.
 */
@RestController
@RequestMapping("/api")
public class CityResource {

    private final Logger log = LoggerFactory.getLogger(CityResource.class);

    @Inject
    private CityRepository cityRepository;

    /**
     * POST  /cities -> Create a new city.
     */
    @RequestMapping(value = "/cities",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody City city) {
        log.debug("REST request to save City : {}", city);
        cityRepository.save(city);
    }

	/**
	 * GET  /cities -> get filtered cities.
	 */
	@RequestMapping(value = "/cities",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public Page<City> getAll(@ModelAttribute("city") City city, Pageable pageable) {
		log.debug("REST request to get filtered Cities");
		return cityRepository.findAll(CitySpecs.getSearchCriteria(city), pageable);
	}

    /**
     * GET  /cities -> get all the cities.
     */
    @RequestMapping(value = "/cities/load",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<City> getAll() {
        log.debug("REST request to get all Cities");
        return cityRepository.findAll();
    }

    /**
     * GET  /cities/:id -> get the "id" city.
     */
    @RequestMapping(value = "/cities/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<City> get(@PathVariable Long id) {
        log.debug("REST request to get City : {}", id);
        return Optional.ofNullable(cityRepository.findOne(id))
            .map(city -> new ResponseEntity<>(
                city,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /cities/:id -> delete the "id" city.
     */
    @RequestMapping(value = "/cities/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete City : {}", id);
        cityRepository.delete(id);
    }
}
