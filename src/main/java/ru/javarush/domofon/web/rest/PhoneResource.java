package ru.javarush.domofon.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.javarush.domofon.domain.Phone;
import ru.javarush.domofon.repository.PhoneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.javarush.domofon.specifications.PhoneSpecs;
import ru.javarush.domofon.web.rest.dto.PhoneSearchDTO;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Phone.
 */
@RestController
@RequestMapping("/api")
public class PhoneResource {

    private final Logger log = LoggerFactory.getLogger(PhoneResource.class);

    @Inject
    private PhoneRepository phoneRepository;

    /**
     * POST  /phones -> Create a new phone.
     */
    @RequestMapping(value = "/phones",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Phone phone) {
        log.debug("REST request to save Phone : {}", phone);
        phoneRepository.save(phone);
    }

    /**
     * GET  /phones -> get all the phones.
     */
    @RequestMapping(value = "/phones",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Page<Phone> getAll(@ModelAttribute("phone") PhoneSearchDTO phone, Pageable pageable) {
        log.debug("REST request to get all Phones");
        return phoneRepository.findAll(PhoneSpecs.getSearchCriteria(phone), pageable);
    }

    /**
     * GET  /phones/:id -> get the "id" phone.
     */
    @RequestMapping(value = "/phones/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Phone> get(@PathVariable Long id) {
        log.debug("REST request to get Phone : {}", id);
        return Optional.ofNullable(phoneRepository.findOne(id))
            .map(phone -> new ResponseEntity<>(
                phone,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /phones/:id -> delete the "id" phone.
     */
    @RequestMapping(value = "/phones/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Phone : {}", id);
        phoneRepository.delete(id);
    }
}
