/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package ru.javarush.domofon.web.rest.dto;
