package ru.javarush.domofon.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.javarush.domofon.domain.Fee;
import ru.javarush.domofon.repository.FeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.javarush.domofon.specifications.FeeSpecs;
import ru.javarush.domofon.web.rest.dto.FeeSearchDTO;

import javax.inject.Inject;
import java.util.Optional;

/**
 * REST controller for managing Fee.
 */
@RestController
@RequestMapping("/api")
public class FeeResource {

    private final Logger log = LoggerFactory.getLogger(FeeResource.class);

    @Inject
    private FeeRepository feeRepository;

    /**
     * POST  /fees -> Create a new fee.
     */
    @RequestMapping(value = "/fees",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Fee fee) {
        log.debug("REST request to save Fee : {}", fee);
        feeRepository.save(fee);
    }

    /**
     * GET  /fees -> get all the fees.
     */
    @RequestMapping(value = "/fees",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Page<Fee> getAll(@ModelAttribute("fee") FeeSearchDTO fee, Pageable pageable) {
        log.debug("REST request to get all Fees");
        return feeRepository.findAll(FeeSpecs.getSearchCriteria(fee), pageable);
    }

    /**
     * GET  /fees/:id -> get the "id" fee.
     */
    @RequestMapping(value = "/fees/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Fee> get(@PathVariable Long id) {
        log.debug("REST request to get Fee : {}", id);
        return Optional.ofNullable(feeRepository.findOne(id))
            .map(fee -> new ResponseEntity<>(
                fee,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /fees/:id -> delete the "id" fee.
     */
    @RequestMapping(value = "/fees/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Fee : {}", id);
        feeRepository.delete(id);
    }
}
