package ru.javarush.domofon.web.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import ru.javarush.domofon.Application;
import ru.javarush.domofon.domain.Affiliate;
import ru.javarush.domofon.repository.AffiliateRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AffiliateResource REST controller.
 *
 * @see AffiliateResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class AffiliateResourceTest {

    private static final String DEFAULT_EXT_CODE = "SAMPLE_TEXT";
    private static final String UPDATED_EXT_CODE = "UPDATED_TEXT";
    private static final String DEFAULT_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_NAME = "UPDATED_TEXT";

    @Inject
    private AffiliateRepository affiliateRepository;

    private MockMvc restAffiliateMockMvc;

    private Affiliate affiliate;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AffiliateResource affiliateResource = new AffiliateResource();
        ReflectionTestUtils.setField(affiliateResource, "affiliateRepository", affiliateRepository);
        this.restAffiliateMockMvc = MockMvcBuilders.standaloneSetup(affiliateResource).build();
    }

    @Before
    public void initTest() {
        affiliate = new Affiliate();
        affiliate.setExtCode(DEFAULT_EXT_CODE);
        affiliate.setName(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createAffiliate() throws Exception {
        // Validate the database is empty
        assertThat(affiliateRepository.findAll()).hasSize(0);

        // Create the Affiliate
        restAffiliateMockMvc.perform(post("/api/affiliates")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(affiliate)))
                .andExpect(status().isOk());

        // Validate the Affiliate in the database
        List<Affiliate> affiliates = affiliateRepository.findAll();
        assertThat(affiliates).hasSize(1);
        Affiliate testAffiliate = affiliates.iterator().next();
        assertThat(testAffiliate.getExtCode()).isEqualTo(DEFAULT_EXT_CODE);
        assertThat(testAffiliate.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void getAllAffiliates() throws Exception {
        // Initialize the database
        affiliateRepository.saveAndFlush(affiliate);

        // Get all the affiliates
        restAffiliateMockMvc.perform(get("/api/affiliates"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].id").value(affiliate.getId().intValue()))
                .andExpect(jsonPath("$.[0].extCode").value(DEFAULT_EXT_CODE.toString()))
                .andExpect(jsonPath("$.[0].name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getAffiliate() throws Exception {
        // Initialize the database
        affiliateRepository.saveAndFlush(affiliate);

        // Get the affiliate
        restAffiliateMockMvc.perform(get("/api/affiliates/{id}", affiliate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(affiliate.getId().intValue()))
            .andExpect(jsonPath("$.extCode").value(DEFAULT_EXT_CODE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAffiliate() throws Exception {
        // Get the affiliate
        restAffiliateMockMvc.perform(get("/api/affiliates/{id}", 1L))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAffiliate() throws Exception {
        // Initialize the database
        affiliateRepository.saveAndFlush(affiliate);

        // Update the affiliate
        affiliate.setExtCode(UPDATED_EXT_CODE);
        affiliate.setName(UPDATED_NAME);
        restAffiliateMockMvc.perform(post("/api/affiliates")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(affiliate)))
                .andExpect(status().isOk());

        // Validate the Affiliate in the database
        List<Affiliate> affiliates = affiliateRepository.findAll();
        assertThat(affiliates).hasSize(1);
        Affiliate testAffiliate = affiliates.iterator().next();
        assertThat(testAffiliate.getExtCode()).isEqualTo(UPDATED_EXT_CODE);
        assertThat(testAffiliate.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void deleteAffiliate() throws Exception {
        // Initialize the database
        affiliateRepository.saveAndFlush(affiliate);

        // Get the affiliate
        restAffiliateMockMvc.perform(delete("/api/affiliates/{id}", affiliate.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Affiliate> affiliates = affiliateRepository.findAll();
        assertThat(affiliates).hasSize(0);
    }
}
