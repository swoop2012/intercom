package ru.javarush.domofon.web.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import ru.javarush.domofon.Application;
import ru.javarush.domofon.domain.Engineer;
import ru.javarush.domofon.repository.EngineerRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EngineerResource REST controller.
 *
 * @see EngineerResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class EngineerResourceTest {

    private static final String DEFAULT_EXT_CODE = "SAMPLE_TEXT";
    private static final String UPDATED_EXT_CODE = "UPDATED_TEXT";

    @Inject
    private EngineerRepository engineerRepository;

    private MockMvc restEngineerMockMvc;

    private Engineer engineer;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        EngineerResource engineerResource = new EngineerResource();
        ReflectionTestUtils.setField(engineerResource, "engineerRepository", engineerRepository);
        this.restEngineerMockMvc = MockMvcBuilders.standaloneSetup(engineerResource).build();
    }

    @Before
    public void initTest() {
        engineer = new Engineer();
        engineer.setExtCode(DEFAULT_EXT_CODE);
    }

    @Test
    @Transactional
    public void createEngineer() throws Exception {
        // Validate the database is empty
        assertThat(engineerRepository.findAll()).hasSize(0);

        // Create the Engineer
        restEngineerMockMvc.perform(post("/api/engineers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(engineer)))
                .andExpect(status().isOk());

        // Validate the Engineer in the database
        List<Engineer> engineers = engineerRepository.findAll();
        assertThat(engineers).hasSize(1);
        Engineer testEngineer = engineers.iterator().next();
        assertThat(testEngineer.getExtCode()).isEqualTo(DEFAULT_EXT_CODE);
    }

    @Test
    @Transactional
    public void getAllEngineers() throws Exception {
        // Initialize the database
        engineerRepository.saveAndFlush(engineer);

        // Get all the engineers
        restEngineerMockMvc.perform(get("/api/engineers"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].id").value(engineer.getId().intValue()))
                .andExpect(jsonPath("$.[0].extCode").value(DEFAULT_EXT_CODE.toString()));
    }

    @Test
    @Transactional
    public void getEngineer() throws Exception {
        // Initialize the database
        engineerRepository.saveAndFlush(engineer);

        // Get the engineer
        restEngineerMockMvc.perform(get("/api/engineers/{id}", engineer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(engineer.getId().intValue()))
            .andExpect(jsonPath("$.extCode").value(DEFAULT_EXT_CODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingEngineer() throws Exception {
        // Get the engineer
        restEngineerMockMvc.perform(get("/api/engineers/{id}", 1L))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEngineer() throws Exception {
        // Initialize the database
        engineerRepository.saveAndFlush(engineer);

        // Update the engineer
        engineer.setExtCode(UPDATED_EXT_CODE);
        restEngineerMockMvc.perform(post("/api/engineers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(engineer)))
                .andExpect(status().isOk());

        // Validate the Engineer in the database
        List<Engineer> engineers = engineerRepository.findAll();
        assertThat(engineers).hasSize(1);
        Engineer testEngineer = engineers.iterator().next();
        assertThat(testEngineer.getExtCode()).isEqualTo(UPDATED_EXT_CODE);
    }

    @Test
    @Transactional
    public void deleteEngineer() throws Exception {
        // Initialize the database
        engineerRepository.saveAndFlush(engineer);

        // Get the engineer
        restEngineerMockMvc.perform(delete("/api/engineers/{id}", engineer.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Engineer> engineers = engineerRepository.findAll();
        assertThat(engineers).hasSize(0);
    }
}
