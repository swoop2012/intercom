package ru.javarush.domofon.web.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import ru.javarush.domofon.Application;
import ru.javarush.domofon.domain.Media;
import ru.javarush.domofon.repository.MediaRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MediaResource REST controller.
 *
 * @see MediaResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class MediaResourceTest {


    private static final Media.Type DEFAULT_TYPE = Media.Type.AUDIO;
    private static final Media.Type UPDATED_TYPE = Media.Type.VIDEO;
    private static final String DEFAULT_FILE_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_FILE_NAME = "UPDATED_TEXT";

    @Inject
    private MediaRepository mediaRepository;

    private MockMvc restMediaMockMvc;

    private Media media;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MediaResource mediaResource = new MediaResource();
        ReflectionTestUtils.setField(mediaResource, "mediaRepository", mediaRepository);
        this.restMediaMockMvc = MockMvcBuilders.standaloneSetup(mediaResource).build();
    }

    @Before
    public void initTest() {
        media = new Media();
        media.setType(DEFAULT_TYPE);
        media.setFileName(DEFAULT_FILE_NAME);
    }

    @Test
    @Transactional
    public void createMedia() throws Exception {
        // Validate the database is empty
        assertThat(mediaRepository.findAll()).hasSize(0);

        // Create the Media
        restMediaMockMvc.perform(post("/api/medias")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(media)))
                .andExpect(status().isOk());

        // Validate the Media in the database
        List<Media> medias = mediaRepository.findAll();
        assertThat(medias).hasSize(1);
        Media testMedia = medias.iterator().next();
        assertThat(testMedia.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testMedia.getFileName()).isEqualTo(DEFAULT_FILE_NAME);
    }

    @Test
    @Transactional
    public void getAllMedias() throws Exception {
        // Initialize the database
        mediaRepository.saveAndFlush(media);

        // Get all the medias
        restMediaMockMvc.perform(get("/api/medias"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].id").value(media.getId().intValue()))
                .andExpect(jsonPath("$.[0].type").value(DEFAULT_TYPE))
                .andExpect(jsonPath("$.[0].fileName").value(DEFAULT_FILE_NAME.toString()));
    }

    @Test
    @Transactional
    public void getMedia() throws Exception {
        // Initialize the database
        mediaRepository.saveAndFlush(media);

        // Get the media
        restMediaMockMvc.perform(get("/api/medias/{id}", media.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(media.getId().intValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.fileName").value(DEFAULT_FILE_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMedia() throws Exception {
        // Get the media
        restMediaMockMvc.perform(get("/api/medias/{id}", 1L))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMedia() throws Exception {
        // Initialize the database
        mediaRepository.saveAndFlush(media);

        // Update the media
        media.setType(UPDATED_TYPE);
        media.setFileName(UPDATED_FILE_NAME);
        restMediaMockMvc.perform(post("/api/medias")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(media)))
                .andExpect(status().isOk());

        // Validate the Media in the database
        List<Media> medias = mediaRepository.findAll();
        assertThat(medias).hasSize(1);
        Media testMedia = medias.iterator().next();
        assertThat(testMedia.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testMedia.getFileName()).isEqualTo(UPDATED_FILE_NAME);
    }

    @Test
    @Transactional
    public void deleteMedia() throws Exception {
        // Initialize the database
        mediaRepository.saveAndFlush(media);

        // Get the media
        restMediaMockMvc.perform(delete("/api/medias/{id}", media.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Media> medias = mediaRepository.findAll();
        assertThat(medias).hasSize(0);
    }
}
