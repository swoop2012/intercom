package ru.javarush.domofon.web.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import ru.javarush.domofon.Application;
import ru.javarush.domofon.domain.Entrance;
import ru.javarush.domofon.repository.EntranceRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EntranceResource REST controller.
 *
 * @see EntranceResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class EntranceResourceTest {

    private static final String DEFAULT_EXT_CODE = "SAMPLE_TEXT";
    private static final String UPDATED_EXT_CODE = "UPDATED_TEXT";
    private static final String DEFAULT_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_NAME = "UPDATED_TEXT";

    private static final Integer DEFAULT_DOMOFON_CODE = 0;
    private static final Integer UPDATED_DOMOFON_CODE = 1;

    private static final Boolean DEFAULT_GATE = false;
    private static final Boolean UPDATED_GATE = true;

    @Inject
    private EntranceRepository entranceRepository;

    private MockMvc restEntranceMockMvc;

    private Entrance entrance;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        EntranceResource entranceResource = new EntranceResource();
        ReflectionTestUtils.setField(entranceResource, "entranceRepository", entranceRepository);
        this.restEntranceMockMvc = MockMvcBuilders.standaloneSetup(entranceResource).build();
    }

    @Before
    public void initTest() {
        entrance = new Entrance();
        entrance.setExtCode(DEFAULT_EXT_CODE);
        entrance.setName(DEFAULT_NAME);
        entrance.setDomofonCode(DEFAULT_DOMOFON_CODE);
        entrance.setGate(DEFAULT_GATE);
    }

    @Test
    @Transactional
    public void createEntrance() throws Exception {
        // Validate the database is empty
        assertThat(entranceRepository.findAll()).hasSize(0);

        // Create the Entrance
        restEntranceMockMvc.perform(post("/api/entrances")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(entrance)))
                .andExpect(status().isOk());

        // Validate the Entrance in the database
        List<Entrance> entrances = entranceRepository.findAll();
        assertThat(entrances).hasSize(1);
        Entrance testEntrance = entrances.iterator().next();
        assertThat(testEntrance.getExtCode()).isEqualTo(DEFAULT_EXT_CODE);
        assertThat(testEntrance.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testEntrance.getDomofonCode()).isEqualTo(DEFAULT_DOMOFON_CODE);
        assertThat(testEntrance.getGate()).isEqualTo(DEFAULT_GATE);
    }

    @Test
    @Transactional
    public void getAllEntrances() throws Exception {
        // Initialize the database
        entranceRepository.saveAndFlush(entrance);

        // Get all the entrances
        restEntranceMockMvc.perform(get("/api/entrances"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].id").value(entrance.getId().intValue()))
                .andExpect(jsonPath("$.[0].extCode").value(DEFAULT_EXT_CODE.toString()))
                .andExpect(jsonPath("$.[0].name").value(DEFAULT_NAME.toString()))
                .andExpect(jsonPath("$.[0].domofonCode").value(DEFAULT_DOMOFON_CODE))
                .andExpect(jsonPath("$.[0].gate").value(DEFAULT_GATE.booleanValue()));
    }

    @Test
    @Transactional
    public void getEntrance() throws Exception {
        // Initialize the database
        entranceRepository.saveAndFlush(entrance);

        // Get the entrance
        restEntranceMockMvc.perform(get("/api/entrances/{id}", entrance.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(entrance.getId().intValue()))
            .andExpect(jsonPath("$.extCode").value(DEFAULT_EXT_CODE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.domofonCode").value(DEFAULT_DOMOFON_CODE))
            .andExpect(jsonPath("$.gate").value(DEFAULT_GATE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingEntrance() throws Exception {
        // Get the entrance
        restEntranceMockMvc.perform(get("/api/entrances/{id}", 1L))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEntrance() throws Exception {
        // Initialize the database
        entranceRepository.saveAndFlush(entrance);

        // Update the entrance
        entrance.setExtCode(UPDATED_EXT_CODE);
        entrance.setName(UPDATED_NAME);
        entrance.setDomofonCode(UPDATED_DOMOFON_CODE);
        entrance.setGate(UPDATED_GATE);
        restEntranceMockMvc.perform(post("/api/entrances")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(entrance)))
                .andExpect(status().isOk());

        // Validate the Entrance in the database
        List<Entrance> entrances = entranceRepository.findAll();
        assertThat(entrances).hasSize(1);
        Entrance testEntrance = entrances.iterator().next();
        assertThat(testEntrance.getExtCode()).isEqualTo(UPDATED_EXT_CODE);
        assertThat(testEntrance.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testEntrance.getDomofonCode()).isEqualTo(UPDATED_DOMOFON_CODE);
        assertThat(testEntrance.getGate()).isEqualTo(UPDATED_GATE);
    }

    @Test
    @Transactional
    public void deleteEntrance() throws Exception {
        // Initialize the database
        entranceRepository.saveAndFlush(entrance);

        // Get the entrance
        restEntranceMockMvc.perform(delete("/api/entrances/{id}", entrance.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Entrance> entrances = entranceRepository.findAll();
        assertThat(entrances).hasSize(0);
    }
}
