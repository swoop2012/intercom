package ru.javarush.domofon.web.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import ru.javarush.domofon.Application;
import ru.javarush.domofon.domain.Street;
import ru.javarush.domofon.repository.StreetRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the StreetResource REST controller.
 *
 * @see StreetResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class StreetResourceTest {

    private static final String DEFAULT_EXT_CODE = "SAMPLE_TEXT";
    private static final String UPDATED_EXT_CODE = "UPDATED_TEXT";
    private static final String DEFAULT_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_NAME = "UPDATED_TEXT";

    @Inject
    private StreetRepository streetRepository;

    private MockMvc restStreetMockMvc;

    private Street street;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        StreetResource streetResource = new StreetResource();
        ReflectionTestUtils.setField(streetResource, "streetRepository", streetRepository);
        this.restStreetMockMvc = MockMvcBuilders.standaloneSetup(streetResource).build();
    }

    @Before
    public void initTest() {
        street = new Street();
        street.setExtCode(DEFAULT_EXT_CODE);
        street.setName(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createStreet() throws Exception {
        // Validate the database is empty
        assertThat(streetRepository.findAll()).hasSize(0);

        // Create the Street
        restStreetMockMvc.perform(post("/api/streets")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(street)))
                .andExpect(status().isOk());

        // Validate the Street in the database
        List<Street> streets = streetRepository.findAll();
        assertThat(streets).hasSize(1);
        Street testStreet = streets.iterator().next();
        assertThat(testStreet.getExtCode()).isEqualTo(DEFAULT_EXT_CODE);
        assertThat(testStreet.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void getAllStreets() throws Exception {
        // Initialize the database
        streetRepository.saveAndFlush(street);

        // Get all the streets
        restStreetMockMvc.perform(get("/api/streets"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].id").value(street.getId().intValue()))
                .andExpect(jsonPath("$.[0].extCode").value(DEFAULT_EXT_CODE.toString()))
                .andExpect(jsonPath("$.[0].name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getStreet() throws Exception {
        // Initialize the database
        streetRepository.saveAndFlush(street);

        // Get the street
        restStreetMockMvc.perform(get("/api/streets/{id}", street.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(street.getId().intValue()))
            .andExpect(jsonPath("$.extCode").value(DEFAULT_EXT_CODE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingStreet() throws Exception {
        // Get the street
        restStreetMockMvc.perform(get("/api/streets/{id}", 1L))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStreet() throws Exception {
        // Initialize the database
        streetRepository.saveAndFlush(street);

        // Update the street
        street.setExtCode(UPDATED_EXT_CODE);
        street.setName(UPDATED_NAME);
        restStreetMockMvc.perform(post("/api/streets")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(street)))
                .andExpect(status().isOk());

        // Validate the Street in the database
        List<Street> streets = streetRepository.findAll();
        assertThat(streets).hasSize(1);
        Street testStreet = streets.iterator().next();
        assertThat(testStreet.getExtCode()).isEqualTo(UPDATED_EXT_CODE);
        assertThat(testStreet.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void deleteStreet() throws Exception {
        // Initialize the database
        streetRepository.saveAndFlush(street);

        // Get the street
        restStreetMockMvc.perform(delete("/api/streets/{id}", street.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Street> streets = streetRepository.findAll();
        assertThat(streets).hasSize(0);
    }
}
