package ru.javarush.domofon.web.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;

import ru.javarush.domofon.Application;
import ru.javarush.domofon.domain.House;
import ru.javarush.domofon.repository.HouseRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the HouseResource REST controller.
 *
 * @see HouseResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class HouseResourceTest {

    private static final String DEFAULT_EXT_CODE = "SAMPLE_TEXT";
    private static final String UPDATED_EXT_CODE = "UPDATED_TEXT";
    private static final String DEFAULT_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_NAME = "UPDATED_TEXT";

    private static final BigDecimal DEFAULT_LATITUDE = BigDecimal.ZERO;
    private static final BigDecimal UPDATED_LATITUDE = BigDecimal.ONE;

    private static final BigDecimal DEFAULT_LONGITUDE = BigDecimal.ZERO;
    private static final BigDecimal UPDATED_LONGITUDE = BigDecimal.ONE;

    private static final Boolean DEFAULT_INACCURATE_ADDRESS = false;
    private static final Boolean UPDATED_INACCURATE_ADDRESS = true;

    private static final Boolean DEFAULT_SCANNED = false;
    private static final Boolean UPDATED_SCANNED = true;

    @Inject
    private HouseRepository houseRepository;

    private MockMvc restHouseMockMvc;

    private House house;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        HouseResource houseResource = new HouseResource();
        ReflectionTestUtils.setField(houseResource, "houseRepository", houseRepository);
        this.restHouseMockMvc = MockMvcBuilders.standaloneSetup(houseResource).build();
    }

    @Before
    public void initTest() {
        house = new House();
        house.setExtCode(DEFAULT_EXT_CODE);
        house.setName(DEFAULT_NAME);
        house.setLatitude(DEFAULT_LATITUDE);
        house.setLongitude(DEFAULT_LONGITUDE);
        house.setInaccurateAddress(DEFAULT_INACCURATE_ADDRESS);
        house.setScanned(DEFAULT_SCANNED);
    }

    @Test
    @Transactional
    public void createHouse() throws Exception {
        // Validate the database is empty
        assertThat(houseRepository.findAll()).hasSize(0);

        // Create the House
        restHouseMockMvc.perform(post("/api/houses")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(house)))
                .andExpect(status().isOk());

        // Validate the House in the database
        List<House> houses = houseRepository.findAll();
        assertThat(houses).hasSize(1);
        House testHouse = houses.iterator().next();
        assertThat(testHouse.getExtCode()).isEqualTo(DEFAULT_EXT_CODE);
        assertThat(testHouse.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testHouse.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testHouse.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testHouse.getInaccurateAddress()).isEqualTo(DEFAULT_INACCURATE_ADDRESS);
        assertThat(testHouse.getScanned()).isEqualTo(DEFAULT_SCANNED);
    }

    @Test
    @Transactional
    public void getAllHouses() throws Exception {
        // Initialize the database
        houseRepository.saveAndFlush(house);

        // Get all the houses
        restHouseMockMvc.perform(get("/api/houses"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].id").value(house.getId().intValue()))
                .andExpect(jsonPath("$.[0].extCode").value(DEFAULT_EXT_CODE.toString()))
                .andExpect(jsonPath("$.[0].name").value(DEFAULT_NAME.toString()))
                .andExpect(jsonPath("$.[0].latitude").value(DEFAULT_LATITUDE.intValue()))
                .andExpect(jsonPath("$.[0].longitude").value(DEFAULT_LONGITUDE.intValue()))
                .andExpect(jsonPath("$.[0].inaccurateAddress").value(DEFAULT_INACCURATE_ADDRESS.booleanValue()))
                .andExpect(jsonPath("$.[0].scanned").value(DEFAULT_SCANNED.booleanValue()));
    }

    @Test
    @Transactional
    public void getHouse() throws Exception {
        // Initialize the database
        houseRepository.saveAndFlush(house);

        // Get the house
        restHouseMockMvc.perform(get("/api/houses/{id}", house.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(house.getId().intValue()))
            .andExpect(jsonPath("$.extCode").value(DEFAULT_EXT_CODE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.intValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.intValue()))
            .andExpect(jsonPath("$.inaccurateAddress").value(DEFAULT_INACCURATE_ADDRESS.booleanValue()))
            .andExpect(jsonPath("$.scanned").value(DEFAULT_SCANNED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingHouse() throws Exception {
        // Get the house
        restHouseMockMvc.perform(get("/api/houses/{id}", 1L))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHouse() throws Exception {
        // Initialize the database
        houseRepository.saveAndFlush(house);

        // Update the house
        house.setExtCode(UPDATED_EXT_CODE);
        house.setName(UPDATED_NAME);
        house.setLatitude(UPDATED_LATITUDE);
        house.setLongitude(UPDATED_LONGITUDE);
        house.setInaccurateAddress(UPDATED_INACCURATE_ADDRESS);
        house.setScanned(UPDATED_SCANNED);
        restHouseMockMvc.perform(post("/api/houses")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(house)))
                .andExpect(status().isOk());

        // Validate the House in the database
        List<House> houses = houseRepository.findAll();
        assertThat(houses).hasSize(1);
        House testHouse = houses.iterator().next();
        assertThat(testHouse.getExtCode()).isEqualTo(UPDATED_EXT_CODE);
        assertThat(testHouse.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testHouse.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testHouse.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testHouse.getInaccurateAddress()).isEqualTo(UPDATED_INACCURATE_ADDRESS);
        assertThat(testHouse.getScanned()).isEqualTo(UPDATED_SCANNED);
    }

    @Test
    @Transactional
    public void deleteHouse() throws Exception {
        // Initialize the database
        houseRepository.saveAndFlush(house);

        // Get the house
        restHouseMockMvc.perform(delete("/api/houses/{id}", house.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<House> houses = houseRepository.findAll();
        assertThat(houses).hasSize(0);
    }
}
