package ru.javarush.domofon.web.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import ru.javarush.domofon.Application;
import ru.javarush.domofon.domain.Defect;
import ru.javarush.domofon.repository.DefectRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DefectResource REST controller.
 *
 * @see DefectResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class DefectResourceTest {

    private static final String DEFAULT_EXT_CODE = "SAMPLE_TEXT";
    private static final String UPDATED_EXT_CODE = "UPDATED_TEXT";
    private static final String DEFAULT_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_NAME = "UPDATED_TEXT";

    @Inject
    private DefectRepository defectRepository;

    private MockMvc restDefectMockMvc;

    private Defect defect;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DefectResource defectResource = new DefectResource();
        ReflectionTestUtils.setField(defectResource, "defectRepository", defectRepository);
        this.restDefectMockMvc = MockMvcBuilders.standaloneSetup(defectResource).build();
    }

    @Before
    public void initTest() {
        defect = new Defect();
        defect.setExtCode(DEFAULT_EXT_CODE);
        defect.setName(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createDefect() throws Exception {
        // Validate the database is empty
        assertThat(defectRepository.findAll()).hasSize(0);

        // Create the Defect
        restDefectMockMvc.perform(post("/api/defects")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(defect)))
                .andExpect(status().isOk());

        // Validate the Defect in the database
        List<Defect> defects = defectRepository.findAll();
        assertThat(defects).hasSize(1);
        Defect testDefect = defects.iterator().next();
        assertThat(testDefect.getExtCode()).isEqualTo(DEFAULT_EXT_CODE);
        assertThat(testDefect.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void getAllDefects() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get all the defects
        restDefectMockMvc.perform(get("/api/defects"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].id").value(defect.getId().intValue()))
                .andExpect(jsonPath("$.[0].extCode").value(DEFAULT_EXT_CODE.toString()))
                .andExpect(jsonPath("$.[0].name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getDefect() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get the defect
        restDefectMockMvc.perform(get("/api/defects/{id}", defect.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(defect.getId().intValue()))
            .andExpect(jsonPath("$.extCode").value(DEFAULT_EXT_CODE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDefect() throws Exception {
        // Get the defect
        restDefectMockMvc.perform(get("/api/defects/{id}", 1L))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDefect() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Update the defect
        defect.setExtCode(UPDATED_EXT_CODE);
        defect.setName(UPDATED_NAME);
        restDefectMockMvc.perform(post("/api/defects")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(defect)))
                .andExpect(status().isOk());

        // Validate the Defect in the database
        List<Defect> defects = defectRepository.findAll();
        assertThat(defects).hasSize(1);
        Defect testDefect = defects.iterator().next();
        assertThat(testDefect.getExtCode()).isEqualTo(UPDATED_EXT_CODE);
        assertThat(testDefect.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void deleteDefect() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get the defect
        restDefectMockMvc.perform(delete("/api/defects/{id}", defect.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Defect> defects = defectRepository.findAll();
        assertThat(defects).hasSize(0);
    }
}
